/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package components;

import beans.Column;
import beans.Table;
import containers.DictionaryPanel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import main.DatabaseConnector;
import main.DatabaseOperations;

/**
 *
 * @author koutsch
 */
public class WorkingPanel extends javax.swing.JPanel {

    DatabaseConnector dc;
    private ButtonGroup searchButtonGroup;
    private String tableName;
    private ColumnPanel[] columnPanels;
    private Table table;
    int columnCount = 0;
    private final DefaultTableModel model = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;   //This causes all cells to be not editable
        }
    };
    private int[] primaryKeyIndices;
    private javax.swing.JTextArea sqlTextArea;
    private javax.swing.JButton addButton;
    private javax.swing.JButton clearInputPanelButton;
    private javax.swing.JButton clearTableSelectionButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JButton searchButton;
    private javax.swing.JButton specialSearchButton;
    private javax.swing.JButton updateButton;
    private javax.swing.JButton executeSQLButton;
    private javax.swing.JButton resultToFileButton;
    private javax.swing.JButton setCellNullButton;

    /**
     * Order by this column.
     */
    String orderByColumn;

    /**
     * This Listener lets the values of the selected row be added to the
     * searchPanels.
     */
    private final ListSelectionListener fillSearchPanels = new ListSelectionListener() {

        @Override
        public void valueChanged(ListSelectionEvent lse) {
            fillSearchPanels();
        }
    };

    // optional parent dictionary panel
    private final DictionaryPanel parent;

    /**
     * Creates new form WorkingPanel
     *
     * @param dc
     * @param parent
     */
    public WorkingPanel(DatabaseConnector dc, DictionaryPanel parent) {
        this.dc = dc;
        initComponents();
        createButtonGroup();
        resultTable.setCellSelectionEnabled(true);
        this.parent = parent;
    }

    private void createButtonGroup() {

        searchButtonGroup = new ButtonGroup();
        searchButtonGroup.add(startsWithRadioButton);
        searchButtonGroup.add(endsWithRadioButton);
        searchButtonGroup.add(containsRadioButton);
        searchButtonGroup.add(exactRadioButton);
    }

    public String getTableName() {

        return tableName;
    }

    public int[] getPrimaryKey() {

        return primaryKeyIndices;
    }

    public JButton getSearchButton() {

        return searchButton;
    }

    /**
     * Get the names of all columns of this WorkingPanel.
     *
     * @return an array with the column names
     */
    public String[] getColumnNames() {

        String[] columnNames = new String[columnCount];
        for (int i = 0; i < columnCount; i++) {

            columnNames[i] = columnPanels[i].getColumnName();
        }
        return columnNames;
    }

    public ColumnPanel[] getColumnPanels() {

        return columnPanels;
    }

    public DefaultTableModel getResultTableModel() {

        return model;
    }

    public JTable getResultTable() {

        return resultTable;
    }

    public JLabel getStateLabel() {

        return stateLabel;
    }

    public String getOrderByColumn() {

        return orderByColumn;
    }

    /**
     *
     * @param tableName
     */
    public void setTableName(String tableName) {

        this.tableName = tableName;
    }

    public void setResultTableSelectionMode(boolean multiple) {

        if (multiple) {
            resultTable.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        } else {
            resultTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        }
    }

    /**
     *
     */
    public void addSqlTextArea() {

        inputPanel.removeAll();
        inputPanel.setLayout(new java.awt.GridLayout());
        upperPanel.remove(searchModePanel);
        searchModePanel.setVisible(false);
        sqlTextArea = new JTextArea(5, 0);
        Font font = new Font(UIManager.getDefaults().getFont("TabbedPane.font").getName(), Font.PLAIN, 18);
        sqlTextArea.setFont(font);
        inputPanel.add(new JScrollPane(sqlTextArea));
        orderByButton.setEnabled(false);
    }

    /**
     *
     */
    public void addButton() {

        URL resource = WorkingPanel.class.getResource("Add_row.png");
        Icon icon = new ImageIcon(resource);
        addButton = new JButton(icon);
        addButton.setToolTipText("Add a new row");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                insert();
            }
        });
        toolBar.add(addButton);
    }

    /**
     *
     */
    public void clearInputPanelButton() {

        URL resource = WorkingPanel.class.getResource("Clear_panels.png");
        Icon icon = new ImageIcon(resource);
        clearInputPanelButton = new JButton(icon);
        clearInputPanelButton.setToolTipText("Clear all search panels");
        clearInputPanelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (ColumnPanel columnPanel : columnPanels) {
                    columnPanel.clearText();
                }
            }
        });
        toolBar.add(clearInputPanelButton);
    }

    /**
     * Makes all selected table rows unselected.
     */
    public void clearTableSelection() {

        resultTable.clearSelection();
    }

    /**
     * Creates a "Clear Table Selection" button.
     */
    public void clearTableSelectionButton() {

        URL resource = WorkingPanel.class.getResource("Clear_table.png");
        Icon icon = new ImageIcon(resource);
        clearTableSelectionButton = new JButton(icon);
        clearTableSelectionButton.setToolTipText("removes all selection of the result table");
        clearTableSelectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearTableSelection();
            }
        });
        toolBar.add(clearTableSelectionButton);
    }

    /**
     *
     */
    public void deleteButton() {

        URL resource = WorkingPanel.class.getResource("Delete_row.png");
        Icon icon = new ImageIcon(resource);
        deleteButton = new JButton(icon);
        deleteButton.setToolTipText("Delete selected row(s)");
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                delete();
            }
        });
        toolBar.add(deleteButton);
    }

    /**
     *
     */
    public void searchButton() {

        URL resource = WorkingPanel.class.getResource("Search.png");
        Icon icon = new ImageIcon(resource);
        searchButton = new JButton(icon);
        searchButton.setToolTipText("Search for entries");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                search();
            }
        });
        toolBar.add(searchButton);
    }

    /**
     *
     */
    public void specialSearchButton() {

        URL resource = WorkingPanel.class.getResource("Special_search.png");
        Icon icon = new ImageIcon(resource);
        specialSearchButton = new JButton(icon);
        specialSearchButton.setToolTipText("Open special search dialog");
        specialSearchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openSpecialSearchDialog();
            }
        });
        toolBar.add(specialSearchButton);
    }

    /**
     *
     */
    public void updateButton() {

        URL resource = WorkingPanel.class.getResource("Update.png");
        Icon icon = new ImageIcon(resource);
        updateButton = new JButton(icon);
        updateButton.setToolTipText("Update selected row(s)");
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                update();
            }
        });
        toolBar.add(updateButton);
    }

    /**
     *
     */
    public void executeSQLButton() {

        URL resource = WorkingPanel.class.getResource("Execute.png");
        Icon icon = new ImageIcon(resource);
        executeSQLButton = new JButton(icon);
        executeSQLButton.setToolTipText("Execute SQL statements (have to be "
                + "separated with ';')");
        executeSQLButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                executeSQL();
            }
        });
        toolBar.add(executeSQLButton);
    }

    /**
     *
     */
    public void resultToFileButton() {

        URL resource = WorkingPanel.class.getResource("To_file.png");
        Icon icon = new ImageIcon(resource);
        resultToFileButton = new JButton(icon);
        resultToFileButton.setToolTipText("the current results from the table"
                + "will be stored in a file");
        resultToFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toFile();
            }
        });
        toolBar.add(resultToFileButton);
    }

    public void setCellNullButton() {

        URL resource = WorkingPanel.class.getResource("Set_null.png");
        Icon icon = new ImageIcon(resource);
        setCellNullButton = new JButton(icon);
        setCellNullButton.setToolTipText("Set selected cell null");
        setCellNullButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                setCellNull();
            }
        });
        toolBar.add(setCellNullButton);
    }

    /**
     * Add rows from a ResultSet to the resultTable.
     *
     * @param res
     * @param model
     * @return an integer representing the inserted row count
     * @throws SQLException
     */
    private int addRowsToResultTable(ResultSet res, DefaultTableModel model) throws SQLException {

        int rowCount = 0;
        for (; res.next();) {

            Object[] rowData = new Object[columnCount];
            for (int i = 0; i < columnCount; i++) {

                rowData[i] = res.getObject(i + 1);
            }
            model.addRow(rowData);
            rowCount++;
        }
        return rowCount;
    }

    private String getSelectedSearchButtonText(ButtonGroup bg) {
        Enumeration<AbstractButton> elements = bg.getElements();
        while (elements.hasMoreElements()) {

            AbstractButton button = elements.nextElement();
            if (button.isSelected()) {

                return button.getText();
            }
        }
        return null;
    }

    /**
     * Get the mode of how to search for entries in the database. 0: entry
     * starts with, 1: entry ends with, 2: entry contains, 3: entry matches
     * exactly
     *
     * @return an integer determining the search mode
     */
    public int getSearchMode() {

        int searchMode = 0;
        String selectedSearchButton = getSelectedSearchButtonText(searchButtonGroup);
        if ("Starts with".equals(selectedSearchButton)) {

            searchMode = DatabaseConnector.STARTS_WITH;
        }

        if ("Ends with".equals(selectedSearchButton)) {

            searchMode = DatabaseConnector.ENDS_WITH;
        }

        if ("Contains".equals(selectedSearchButton)) {

            searchMode = DatabaseConnector.CONTAINS;
        }

        if ("Exact Match".equals(selectedSearchButton)) {

            searchMode = DatabaseConnector.EXACT;
        }
        return searchMode;
    }

    /**
     * Retrieves the column names and values from the non-empty column panels.
     *
     * @return
     */
    public HashMap<String, Object> getValuesToSearchFor() {

        HashMap<String, Object> columnsToSearch = new HashMap<>();
        for (ColumnPanel columnPanel : columnPanels) {
            if (!columnPanel.getText().isEmpty()) {
                columnsToSearch.put(columnPanel.getColumnName(), columnPanel.getText());
            }
        }
        return columnsToSearch;
    }

    /**
     * Performs a search. At first all existing rows are deleted, then it looks
     * for what is searched. After that it going to the database and when
     * there's data coming back it will be displayed in the result table.
     */
    public void search() {
        try {
            model.setRowCount(0);

            ResultSet res = dc.search(tableName, getValuesToSearchFor(), getSearchMode(), orderByColumn, null);
            int rowCount = addRowsToResultTable(res, model);
            stateLabel.setText("Matching records found: " + rowCount);

        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Basic elements not loaded! Check XML file!\n"
                    + ex, "Error", JOptionPane.ERROR_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    /**
     * Searches for a certain column in the result table.
     *
     * @param columnToRetrieve the name of the column to get data from
     * @return an Object[] holding the column values, null if there are no
     * search parameters in the search panels
     *
     * @throws SQLException
     */
    public Object[] search(String columnToRetrieve) throws SQLException {

        HashMap<String, Object> valuesToSearchFor = getValuesToSearchFor();
        if (!valuesToSearchFor.isEmpty()) {

            ResultSet res = dc.search(tableName, getValuesToSearchFor(), getSearchMode(), null, columnToRetrieve);
            res.last();
            int rowCount = res.getRow();

            res.beforeFirst();
            Object[] rows = new Object[rowCount];
            for (int i = 0; res.next(); i++) {

                rows[i] = res.getObject(1);
            }
            return rows;
        } else {
            return null;
        }
    }

    /**
     * Gets a cell from a row with a certain row index.
     *
     * @param columnName the name of the column
     * @return an Object representing the table row data
     */
    public Object getCellFromSelected(String columnName) {

        if (resultTable.getSelectedRowCount() != 0) {

            int rowIndex = resultTable.getSelectedRow();

            int columnIndex = model.findColumn(columnName);
            Object cell = resultTable.getValueAt(rowIndex, columnIndex);
            return cell;
        } else {

            return null;
        }
    }

    /**
     * Gets cells from rows with a certain row index.
     *
     * @param columnName the name of the column
     * @return an Object[] representing the table row data
     */
    public Object[] getCellsFromSelected(String columnName) {

        if (resultTable.getSelectedRowCount() != 0) {

            int[] rowIndexes = resultTable.getSelectedRows();
            Object[] cells = new Object[rowIndexes.length];
            for (int i = 0; i < rowIndexes.length; i++) {

                int columnIndex = model.findColumn(columnName);
                cells[i] = resultTable.getValueAt(rowIndexes[i], columnIndex);
            }
            return cells;
        } else {

            return null;
        }
    }

    /**
     * Inserts a single record (row) into the database. The data in the
     * columnSearchFields are inserted into the respective columns. If the user
     * doesn't fill out a text field than a null value in the respective cell is
     * created.
     *
     * To make it database system independent the inserted value is not shown in
     * the resultTable since different system use different commands to retrieve
     * the last inserted row. Since the delete and update methods use (auto
     * generated) primary keys to assure that the right record is chosen it
     * causes a bug when using the user data filled in at GUI level in the
     * resultTable (the primary keys are just created at database level)... Keep
     * it simple...
     */
    private void insert() {
        String autoIncrementColumnName = table.getAutoIncrementColumnName();
        String insert = DatabaseOperations.insert(columnPanels, dc, tableName, model,
                autoIncrementColumnName);
        stateLabel.setText(insert);
    }

    /**
     * Deletes the selected rows. If no rows are selected this method prompts
     * the user to select at least one row. After pressing the 'delete' button a
     * "Are you sure?"-option pane occurs.
     */
    private void delete() {

        String deleteState = DatabaseOperations.delete(resultTable, tableName, dc, table, model);
        stateLabel.setText(deleteState);

        if (parent != null) {

            parent.displayDependingResults();
        }
    }

    /**
     * Performs an update statement. Those columnTextFields which are not empty
     * will be recognized and the updates will performed at all resultTable rows
     * selected. After that a search is performed which results in searching for
     * all rows matching the updated column cells. The row selected is removed
     * as well.
     */
    private void update() {

        String updateState = DatabaseOperations.update(resultTable, tableName, dc, primaryKeyIndices, columnPanels);
        clearTableSelection();
        stateLabel.setText(updateState);

        if (parent != null) {

            parent.displayDependingResults();
        }
    }

    private void setCellNull() {
        try {
            int selectedColumn = resultTable.getSelectedColumn();
            String columnName = model.getColumnName(selectedColumn);
            DatabaseOperations.update(resultTable, tableName, dc, table, null, columnName);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    /**
     *
     *
     * @param res
     * @return
     */
    public int display(ResultSet res) {
        try {
            model.setColumnCount(0);
            model.setRowCount(0);
            columnCount = res.getMetaData().getColumnCount();
            for (int i = 0; i < columnCount; i++) {

                model.addColumn(res.getMetaData().getColumnName(i + 1));
            }

            int rowCount = addRowsToResultTable(res, model);
            stateLabel.setText("Matching records found: " + rowCount);
            return rowCount;

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
            return 0;
        }
    }

    /**
     * Adds or removes an ListSelectionListener to the resultTable so that the
     * values of the selected table row are displayed in the searchPanels.
     *
     * @param enabled if true the Listener is added, otherwise removed
     */
    public void fillSearchPanelsWithSelectedRowValues(boolean enabled) {

        if (enabled) {

            resultTable.getSelectionModel().addListSelectionListener(fillSearchPanels);
        } else {

            resultTable.getSelectionModel().removeListSelectionListener(fillSearchPanels);
        }
    }

    /**
     * Fills the searchPanels with the data from the selected resultTable row
     * the respective cell value is a string.
     */
    private void fillSearchPanels() {

        int selectedRow = resultTable.getSelectedRow();
        if (selectedRow != -1) {

            for (int i = 0; i < columnCount; i++) {

                columnPanels[i].setText("");
                Object valueAt = resultTable.getValueAt(selectedRow, i);
                if (valueAt != null && valueAt.getClass().equals(String.class)) {

                    columnPanels[i].setText(valueAt.toString());
                }
            }
        }
    }

    /**
     * Executes an SQL statements. If there are more than one statement they
     * have to be separated by ';'.
     *
     */
    private void executeSQL() {
        try {

            String text = sqlTextArea.getText();
            String[] lines = text.split(";");

            for (String line : lines) {

                ResultSet res = dc.execute(line + ";");
                if (res != null) {

                    display(res);
                }
            }
            dc.commit();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
            try {
                dc.rollback();
            } catch (SQLException ex1) {
                JOptionPane.showMessageDialog(this, ex1);
            }
        }
    }

    /**
     * Print results to a file. The file will be give the current date. Should
     * be better!
     */
    private void toFile() {
        try {
            java.util.Date date = new java.util.Date();
            String fileName = "Query_Result_" + date.toString();
            java.io.File file = new java.io.File(fileName);
            try (java.io.FileWriter write = new java.io.FileWriter(file)) {
                for (int colName = 0; colName < model.getColumnCount(); colName++) {

                    write.append(model.getColumnName(colName) + ":\t\t");
                }
                write.append("\n");

                for (int row = 0; row < model.getRowCount(); row++) {

                    for (int col = 0; col < model.getColumnCount(); col++) {

                        write.append(model.getValueAt(row, col) + "\t\t");
                    }
                    write.append("\n");
                }
            }

        } catch (java.io.IOException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    /**
     * Clears the whole GUI from loaded data.
     */
    private void clearGUI() {

        model.setColumnCount(0);
        model.setRowCount(0);
        inputPanel.removeAll();
        columnPanels = null;
        stateLabel.setText("");
    }

    private void openSpecialSearchDialog() {

        dialogs.SpecialSearchDialog ssd = new dialogs.SpecialSearchDialog(this, true);
    }

    /**
     * Searches for null values in a certain column. Refer to DatabaseConnector!
     *
     * @param columnName the name of the column to look through
     */
    public void searchEmptyCells(String columnName) {
        try {
            model.setRowCount(0);

            ResultSet res = dc.searchForEmptyCells(tableName, columnName);
            addRowsToResultTable(res, model);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    /**
     * Searches for multiple occurring records in a certain column. Refer to
     * DatabaseConnector!
     *
     * @param columnName the name of the column to look through
     */
    public void searchDuplicates(String columnName) {
        try {

            model.setRowCount(0);

            ResultSet res = dc.searchForDuplicates(tableName, columnName);
            addRowsToResultTable(res, model);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    /**
     * Creates text fields labeled with the column names where the user can
     * enter to search and insert entries. Its quiet a fat method, at first
     * clears everything and then does some recursive stuff, you know.. But it's
     * not that hard to find out how it works.
     *
     * Note that it uses the .getColumnInfoSqlite method of DatabaseConnector.
     * If this program will be adapted to another DB system, this has to be
     * replaced by the proper method (refer to class DatabaseConnector)!!!
     */
    public void populate() {
        try {

            // store column name (String) and primary key (boolean) info in Object[][]
            // should work with Postgres as well --> DatabaseConnector!
            table = dc.getColumnInfo(tableName);
            columnCount = table.columnCount();

            if (columnCount > 0) {

                // prepare the GUI for "table mode"
                clearGUI();

                // get the number of columns and prepare a storage for column information            
                Column[] columns = table.getColumns();

                orderByColumn = columns[0].getName();

                // prepare a storage for the column search GUI panel and set panel layout
                columnPanels = new ColumnPanel[columnCount];

                // if there are more than 4 columns, the panel is splitted into two rows
                if (columnCount > 4) {

                    inputPanel.setLayout(new java.awt.GridLayout(2, 0));
                } else {

                    inputPanel.setLayout(new java.awt.GridLayout());
                }

                // loop through the columns to:
                // 1) to store the column information from the database in the Column objects
                // 2) get the indexes of the primary key columns
                // 3) create the panels and text fields for the column search and manipulation function
                // 4) add 3) to the searchPanel
                for (int i = 0; i < columnCount; i++) {

                    columnPanels[i] = new ColumnPanel(columns[i].getName());
                    inputPanel.add(columnPanels[i]);

                    model.addColumn(columns[i].getName());
                }

                // get the column index of the primary keys
                primaryKeyIndices = table.getPrimaryKeyIndices();

                // make tableName border title and update the searchPanel to make the new component visible
                inputPanel.updateUI();
            } else {

                JOptionPane.showMessageDialog(this, "Table is empty!");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    public void tableModeButtons() {

        searchButton();
        specialSearchButton();
        addButton();
        clearInputPanelButton();
        clearTableSelectionButton();
        resultToFileButton();
        updateButton();
        deleteButton();
        setCellNullButton();
    }

    private void changeOrderByColumn() {

        Object cn = JOptionPane.showInputDialog(this, "Choose a column:",
                "Order By Column", JOptionPane.QUESTION_MESSAGE, null, table.getColumnNames(), null);
        if (cn != null) {

            orderByColumn = (String) cn;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        stateLabel = new javax.swing.JLabel();
        outputScrollPane = new javax.swing.JScrollPane();
        resultTable = new javax.swing.JTable();
        upperPanel = new javax.swing.JPanel();
        inputPanel = new javax.swing.JPanel();
        searchModePanel = new javax.swing.JPanel();
        startsWithRadioButton = new javax.swing.JRadioButton();
        endsWithRadioButton = new javax.swing.JRadioButton();
        containsRadioButton = new javax.swing.JRadioButton();
        exactRadioButton = new javax.swing.JRadioButton();
        fillWithSelectionCheckBox = new javax.swing.JCheckBox();
        toolBar = new javax.swing.JToolBar();
        orderByButton = new javax.swing.JButton();

        resultTable.setFont(resultTable.getFont().deriveFont(resultTable.getFont().getStyle() | java.awt.Font.BOLD, resultTable.getFont().getSize()+1));
        resultTable.setModel(model);
        resultTable.setRowHeight(24);
        resultTable.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        resultTable.getTableHeader().setReorderingAllowed(false);
        outputScrollPane.setViewportView(resultTable);

        upperPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        inputPanel.setBackground(new java.awt.Color(68, 68, 68));
        inputPanel.setBorder(null);
        inputPanel.setForeground(new java.awt.Color(254, 254, 254));

        javax.swing.GroupLayout inputPanelLayout = new javax.swing.GroupLayout(inputPanel);
        inputPanel.setLayout(inputPanelLayout);
        inputPanelLayout.setHorizontalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        inputPanelLayout.setVerticalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 90, Short.MAX_VALUE)
        );

        searchModePanel.setBackground(new java.awt.Color(246, 238, 123));
        searchModePanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        startsWithRadioButton.setForeground(new java.awt.Color(1, 1, 1));
        startsWithRadioButton.setText("S");
        startsWithRadioButton.setToolTipText("Search for entries beginning with search expression");

        endsWithRadioButton.setForeground(new java.awt.Color(1, 1, 1));
        endsWithRadioButton.setText("E");
        endsWithRadioButton.setToolTipText("Search for entries ending with search expression");

        containsRadioButton.setForeground(new java.awt.Color(1, 1, 1));
        containsRadioButton.setText("C");
        containsRadioButton.setToolTipText("Search for entries containing the search expression");

        exactRadioButton.setForeground(new java.awt.Color(1, 1, 1));
        exactRadioButton.setText("M");
        exactRadioButton.setToolTipText("Search for exact match of expression");

        fillWithSelectionCheckBox.setForeground(new java.awt.Color(1, 1, 1));
        fillWithSelectionCheckBox.setText("Fill");
        fillWithSelectionCheckBox.setToolTipText("Fill search panels with data from selected row (String only)");
        fillWithSelectionCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fillWithSelectionCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout searchModePanelLayout = new javax.swing.GroupLayout(searchModePanel);
        searchModePanel.setLayout(searchModePanelLayout);
        searchModePanelLayout.setHorizontalGroup(
            searchModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchModePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(startsWithRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(endsWithRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(containsRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exactRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fillWithSelectionCheckBox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        searchModePanelLayout.setVerticalGroup(
            searchModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchModePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(startsWithRadioButton)
                .addComponent(endsWithRadioButton)
                .addComponent(containsRadioButton)
                .addComponent(exactRadioButton)
                .addComponent(fillWithSelectionCheckBox))
        );

        toolBar.setFloatable(false);
        toolBar.setRollover(true);
        toolBar.setBorderPainted(false);

        javax.swing.GroupLayout upperPanelLayout = new javax.swing.GroupLayout(upperPanel);
        upperPanel.setLayout(upperPanelLayout);
        upperPanelLayout.setHorizontalGroup(
            upperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(searchModePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(inputPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        upperPanelLayout.setVerticalGroup(
            upperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(upperPanelLayout.createSequentialGroup()
                .addComponent(inputPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchModePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        orderByButton.setText("Order By");
        orderByButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                orderByButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(outputScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
                    .addComponent(upperPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(stateLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(orderByButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(upperPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(outputScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(orderByButton)
                    .addComponent(stateLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void fillWithSelectionCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fillWithSelectionCheckBoxActionPerformed
        // TODO add your handling code here:
        if (fillWithSelectionCheckBox.isSelected()) {

            this.fillSearchPanelsWithSelectedRowValues(true);
        } else {

            this.fillSearchPanelsWithSelectedRowValues(false);
        }
    }//GEN-LAST:event_fillWithSelectionCheckBoxActionPerformed

    private void orderByButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_orderByButtonActionPerformed
        // TODO add your handling code here:
        changeOrderByColumn();
    }//GEN-LAST:event_orderByButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton containsRadioButton;
    private javax.swing.JRadioButton endsWithRadioButton;
    private javax.swing.JRadioButton exactRadioButton;
    private javax.swing.JCheckBox fillWithSelectionCheckBox;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JButton orderByButton;
    private javax.swing.JScrollPane outputScrollPane;
    private javax.swing.JTable resultTable;
    private javax.swing.JPanel searchModePanel;
    private javax.swing.JRadioButton startsWithRadioButton;
    private javax.swing.JLabel stateLabel;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JPanel upperPanel;
    // End of variables declaration//GEN-END:variables
}
