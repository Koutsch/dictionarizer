/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package containers;

import beans.DescriptionTables;
import beans.ForeignKeyColumn;
import beans.Table;
import components.WorkingPanel;
import dialogs.ExtraMtoNDescriptionDialog;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.ParserConfigurationException;
import main.DatabaseConnector;
import main.DatabaseOperations;
import main.XmlParser;
import org.xml.sax.SAXException;

/**
 * @author koutsch
 */
public class DictionaryPanel extends javax.swing.JPanel {

    // essential components
    MainFrame parent;
    DatabaseConnector dc;

    WorkingPanel mainWork;
    String mainTableName;
    String mainTableId;
    String mainView;

    WorkingPanel[] descriptionWorks;
    JScrollPane[] descriptionScrollPanes;
    String[] descriptionTables;
    String[] descriptionTableIds;
    String[] tablesHoldingkeyColumnsForSearch;
    String[] keyColumnsForSearch;
    String[] mToNTables;
    String[] descriptionViews;

    String xmlFile;

    // word table column descriptors
    int[] mainPrimaryKeyIndices;

    // Variables for components depending on selected Panel of tabbedPane.
    private String tableToSearchFor;
    private String keyToSearchFor;
    private String columnHoldingKeyValue;
    private String dependingView;
    private String descriptionTableName;
    private WorkingPanel work;
    private String mToNTable;
    private ForeignKeyColumn[] referencedTablesAndColumns;
    private WorkingPanel wpM;
    private WorkingPanel wpN;

    //extra m:n column
    private String[][] extraMtoNdescriptionColumnCollection;
    private String[] extraMtoNdescriptionColumns;

    private final DefaultTableModel dependentResultTableModel = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;   //This causes all cells to be not editable
        }
    };
    private Table mainTable;

    /**
     * Creates new form DictionaryPanel
     *
     * @param parent
     * @param dc
     * @param xmlFile
     */
    public DictionaryPanel(MainFrame parent, DatabaseConnector dc, String xmlFile) {
        try {
            this.parent = parent;
            this.dc = dc;
            this.xmlFile = xmlFile;
            initComponents();
            mainSplitPane.setDividerLocation(parent.getWidth() / 2);
            resultSplitPane.setDividerLocation((parent.getHeight() / 3) * 2);
            populate();
            setPanelToWorkWith();
            wpM = mainWork;
            wpN = work;

            setMnemonicsAndTooltips();

            dependentResultTable.setCellSelectionEnabled(true);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(parent, "Review XML file! Crucial data missing!\n" + ex, "Error", JOptionPane.ERROR_MESSAGE);
        } catch (ParserConfigurationException | SAXException | IOException | SQLException ex) {
            JOptionPane.showMessageDialog(parent, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Sets the Mnemonics for the buttonPanel buttons.
     */
    private void setMnemonicsAndTooltips() {

        insertButton.setToolTipText("Insert a new entry. (ALT + I)");
        insertButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt I"), "insert");
        insertButton.getActionMap().put("insert",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        insertInMain();
                    }
                });

        updateButton.setToolTipText("Update selected entries. (ALT + U)");
        updateButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt U"), "update");
        updateButton.getActionMap().put("update",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        updateMain();
                    }
                });

        deleteButton.setToolTipText("Delete selected entries. (ALT + D)");
        deleteButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt D"), "delete");
        deleteButton.getActionMap().put("delete",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        deleteFromMain();
                    }
                });

        searchForButton.setToolTipText("Search for entries related to the slected entries below. (ALT + S)");
        searchForButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt S"), "searchFor");
        searchForButton.getActionMap().put("searchFor",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        searchFor();
                    }
                });

        toOrFromMainToggleButton.setToolTipText("Change in which table you can make multiple selections. (ALT + M)");
        toOrFromMainToggleButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt M"), "toFromMain");
        toOrFromMainToggleButton.getActionMap().put("toFromMain",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        changeToggleButton();
                    }
                });

        connectButton.setToolTipText("Connect main table entries with description table entries. (ALT + C)");
        connectButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt C"), "connect");
        connectButton.getActionMap().put("connect",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        connectWith();
                    }
                });

        deleteConnectionButton.setToolTipText("Delete connections between main and description tables. (ALT + B)");
        deleteConnectionButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt B"), "deleteCon");
        deleteConnectionButton.getActionMap().put("deleteCon",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        removeConnection();
                    }
                });

        tabbedPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt LEFT"), "prev");
        tabbedPane.getActionMap().put("prev", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tabbedPane.getSelectedIndex() > 0) {
                    tabbedPane.setSelectedIndex(tabbedPane.getSelectedIndex() - 1);
                }
            }
        });

        tabbedPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt RIGHT"), "next");
        tabbedPane.getActionMap().put("next", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (tabbedPane.getSelectedIndex() < tabbedPane.getComponentCount() - 1) {
                    tabbedPane.setSelectedIndex(tabbedPane.getSelectedIndex() + 1);
                }
            }
        });

        toDescriptionButton.setToolTipText("Search for row selected in the dependent result table. (ALT + W)");
        toDescriptionButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt W"), "toDescription");
        toDescriptionButton.getActionMap().put("toDescription", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                toDescription();
            }
        });

        allToDescriptionButton.setToolTipText("Search for row selected in the dependent result table. (ALT + Q)");
        allToDescriptionButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt Q"), "allToDescription");
        toDescriptionButton.getActionMap().put("allToDescription", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                allToDescription();
            }
        });

        extraColumnButton.setToolTipText("Open a dialog where you can edit an additional column of the connecting m:n-table. (ALT + A)");
        extraColumnButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt A"), "extraColumn");
        extraColumnButton.getActionMap().put("extraColumn", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                extraMtoNColumn();
            }
        });

        searchForEmptyButton.setToolTipText("Search for all entries which have no connection to any description table entries. (ALT + E)");
        searchForEmptyButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt E"), "searchEmpty");
        searchForEmptyButton.getActionMap().put("searchEmpty", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                searchForEmpty();
            }
        });

        mainWork.getSearchButton().setToolTipText("Search for entries. (ALT + X)");
        mainWork.getSearchButton().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("alt X"), "searchMain");
        mainWork.getSearchButton().getActionMap().put("searchMain", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mainWork.search();
            }
        });
    }

    /**
     * Reads from XML file and sets the respective description panels.
     */
    private void populate() throws ParserConfigurationException, SAXException, IOException, SQLException {

        XmlParser xp = new XmlParser(xmlFile);
        String[] mts = xp.getMainTableSpecifications();

        // hier müssen auch die extra m:n columns geladen werden und dann zur jeweiligen description table zugeordnet werden (ins array)
        //main table
        mainTableName = mts[0];
        mainTableId = mts[1];
        mainView = mts[2];
        // Object[] tableColumnInfo = DatabaseOperations.getTableColumnInfo(dc, mainTable);
        mainTable = dc.getColumnInfo(mainTableName);
        mainPrimaryKeyIndices = mainTable.getPrimaryKeyIndices();
        addMainWorkingPanel(dc, mts[2]);

        //description tables
        DescriptionTables dt = xp.getDescriptionTableSpecifications();
        String[][] dts = dt.getDescriptionTableData();
        extraMtoNdescriptionColumnCollection = dt.getExtradescriptionColumnData();
        int dtCount = dts.length;
        descriptionWorks = new WorkingPanel[dtCount];
        descriptionScrollPanes = new JScrollPane[dtCount];
        descriptionTables = new String[dtCount];
        descriptionTableIds = new String[dtCount];
        keyColumnsForSearch = new String[dtCount];
        tablesHoldingkeyColumnsForSearch = new String[dtCount];
        mToNTables = new String[dtCount];
        descriptionViews = new String[dtCount];
        for (int i = 0; i < dtCount; i++) {

            descriptionWorks[i] = new WorkingPanel(dc, this);
            descriptionWorks[i].setTableName(dts[i][0]);
            descriptionWorks[i].populate();
            if (dts[i][6].equals(XmlParser.searchMode)) {

                descriptionWorks[i].searchButton();
                descriptionWorks[i].clearInputPanelButton();
                descriptionWorks[i].clearTableSelectionButton();
            }
            if (dts[i][6].equals(XmlParser.searchEditMode)) {

                descriptionWorks[i].searchButton();
                descriptionWorks[i].clearInputPanelButton();
                descriptionWorks[i].clearTableSelectionButton();
                descriptionWorks[i].addButton();
                descriptionWorks[i].updateButton();
            }
            descriptionWorks[i].setResultTableSelectionMode(false);
            descriptionScrollPanes[i] = new JScrollPane();
            descriptionScrollPanes[i].setViewportView(descriptionWorks[i]);
            tabbedPane.add(dts[i][5], descriptionScrollPanes[i]);

            descriptionTables[i] = dts[i][0];
            descriptionTableIds[i] = dts[i][1];
            keyColumnsForSearch[i] = dts[i][2];
            mToNTables[i] = dts[i][3];
            descriptionViews[i] = dts[i][4];
            if (mToNTables[i] == null) {

                tablesHoldingkeyColumnsForSearch[i] = mainTableName;
            } else {

                tablesHoldingkeyColumnsForSearch[i] = descriptionViews[i];
            }
        }
    }

    /**
     * Adds the main working panel and sets it up.
     *
     * @param dc the DatabaseConnector to use
     * @param mainTableView the name of the view for the main table
     */
    private void addMainWorkingPanel(DatabaseConnector dc, String mainTableView) {

        mainWork = new WorkingPanel(dc, this);
        mainWork.setTableName(mainTableView);
        mainWork.populate();
        mainWork.searchButton();
        mainWork.specialSearchButton();
        mainWork.clearInputPanelButton();
        mainWork.clearTableSelectionButton();

        mainWork.getResultTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if (!lse.getValueIsAdjusting()) {

                    displayDependingResults();
                }
            }
        });
        wordScrollPane.setViewportView(mainWork);
    }

    /**
     * Calling this method sets the WorkingPanel selected in the tabbedPane as
     * connected to the main panel.
     */
    private void setPanelToWorkWith() {

        int stp = tabbedPane.getSelectedIndex();

        dependingView = descriptionViews[stp];
        tableToSearchFor = tablesHoldingkeyColumnsForSearch[stp];
        keyToSearchFor = keyColumnsForSearch[stp];
        columnHoldingKeyValue = descriptionTableIds[stp];
        work = descriptionWorks[stp];
        mToNTable = mToNTables[stp];
        descriptionTableName = descriptionTables[stp];

        extraMtoNdescriptionColumns = extraMtoNdescriptionColumnCollection[stp];
        if (extraMtoNdescriptionColumns != null) {

            extraColumnButton.setEnabled(true);
        } else {

            extraColumnButton.setEnabled(false);
        }
    }

    /**
     * Displays the results connected to the results in the main table.
     */
    public void displayDependingResults() {

        dependentResultTableModel.setColumnCount(0);
        dependentResultTableModel.setRowCount(0);

        setPanelToWorkWith();

        if (mainWork != null) {

            if (mainWork.getResultTable().getSelectedRow() != -1) {
                try {

                    if (dependingView != null) {

                        int wordId = mainWork.getResultTable().getColumnModel().getColumnIndex(mainTableId);
                        Object selectedValue = mainWork.getResultTable().getValueAt(mainWork.getResultTable().getSelectedRow(), wordId);

                        ResultSet res = dc.search(dependingView, mainTableId, selectedValue, null);

                        ResultSetMetaData meta = res.getMetaData();
                        int columnCount = meta.getColumnCount();

                        for (int i = 0; i < columnCount; i++) {

                            dependentResultTableModel.addColumn(meta.getColumnName(i + 1));
                        }

                        for (; res.next();) {

                            Object[] rowData = new Object[columnCount];
                            for (int i = 0; i < columnCount; i++) {

                                rowData[i] = res.getObject(i + 1);
                            }
                            dependentResultTableModel.addRow(rowData);
                        }
                    }
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(parent, ex + "\nTable information seems to be invalid!", "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }

    /**
     * Inserts values to the main table.
     */
    private void insertInMain() {
        String autoIncrementColumnName = mainTable.getAutoIncrementColumnName();
        String insert = DatabaseOperations.insert(mainWork.getColumnPanels(), dc, mainTableName,
                mainWork.getResultTableModel(), autoIncrementColumnName);
        mainWork.getStateLabel().setText(insert);
    }

    /**
     * Updates main table rows. The table selections are removed.
     */
    private void updateMain() {

        String updateState = DatabaseOperations.update(mainWork.getResultTable(), mainTableName,
                dc, mainPrimaryKeyIndices, mainWork.getColumnPanels());
        mainWork.getStateLabel().setText(updateState);
    }

    /**
     * Delete main table rows.
     */
    private void deleteFromMain() {

        String deleteState = DatabaseOperations.delete(mainWork.getResultTable(), mainTableName,
                dc, mainTable, mainWork.getResultTableModel());
        mainWork.getStateLabel().setText(deleteState);
    }

    /**
     * Search for main table records by related description panel rows.
     */
    private void searchFor() {
        try {

            Object[] selectedRowCells;
            if (fromPanelCheckBox.isSelected()) {

                selectedRowCells = work.search(columnHoldingKeyValue);

            } else {

                selectedRowCells = work.getCellsFromSelected(columnHoldingKeyValue);
            }

            if (selectedRowCells != null) {

                if (selectedRowCells.length > 0) {

                    String orderByColumn = mainWork.getOrderByColumn();

                    ResultSet res;
                    if (combinedCheckBox.isSelected()) {

                        res = dc.combinedSearch(mainView, selectedRowCells, mainTableId, tableToSearchFor,
                                keyToSearchFor, mainTableId, mainWork.getValuesToSearchFor(),
                                mainWork.getSearchMode(), orderByColumn);

                    } else {
                        res = dc.searchByRelatedTable(mainView, mainTableId,
                                mainTableId, tableToSearchFor,
                                keyToSearchFor, selectedRowCells, orderByColumn);
                    }
                    mainWork.display(res);
                }
            } else {

                String messageText;
                if (fromPanelCheckBox.isSelected()) {

                    messageText = "No search parameters entered!";
                } else {

                    messageText = "Select at least one row!";
                }

                JOptionPane.showMessageDialog(parent, messageText,
                        "No selection made", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(parent, "Basic elements not loaded! Check XML file!\n"
                    + ex, "Error", JOptionPane.ERROR_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(parent, "An error occurred!\n" + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Search for main table entries which are not connected to the current
     * description table.
     */
    private void searchForEmpty() {
        try {

            String mnfk = null;
            ForeignKeyColumn[] rtacns = dc.getReferencedTableAndColumnNames(mToNTable);
            for (ForeignKeyColumn rtacn : rtacns) {
                if (rtacn.getReferencedColumnName().equals(mainTableId)) {
                    mnfk = rtacn.getColumnName();
                }
            }

            String orderByColumn = mainWork.getOrderByColumn();

            if (mToNTable != null && mnfk != null) {

                ResultSet res = dc.searchForNotRelatedTables(mainView, mainTableId, mnfk, mToNTable, orderByColumn);
                mainWork.display(res);
            } else {

                ResultSet res = dc.searchForEmptyFKs(mainView, mainTableId, mainTableName, keyToSearchFor, orderByColumn);
                mainWork.display(res);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(parent, "An error occurred!\n" + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Change the working direction. Toggle button text is changed as well as
     * the selection modes of main and description tables.
     */
    private void changeToggleButton() {

        if (toOrFromMainToggleButton.isSelected()) {

            toOrFromMainToggleButton.setText("From Main to...");
            mainWork.setResultTableSelectionMode(false);
            for (WorkingPanel descriptionWork : descriptionWorks) {

                descriptionWork.setResultTableSelectionMode(true);
            }

        } else {

            toOrFromMainToggleButton.setText("To Main from...");
            mainWork.setResultTableSelectionMode(true);
            for (WorkingPanel descriptionWork : descriptionWorks) {

                descriptionWork.setResultTableSelectionMode(false);
            }
        }
    }

    /**
     * Connect a description row(s) with main table row(s). The main table
     * selection is cleared.
     */
    private void connectWith() {

        setPanelToWorkWith();
        try {
            if (mToNTable != null) {

                Object columnM;
                Object columnN;
                Object refColumnM;
                Object refColumnN;

                referencedTablesAndColumns = dc.getReferencedTableAndColumnNames(mToNTable);

                if (toOrFromMainToggleButton.isSelected()) {
                    columnM = referencedTablesAndColumns[1].getColumnName();
                    columnN = referencedTablesAndColumns[0].getColumnName();
                    refColumnM = referencedTablesAndColumns[1].getReferencedColumnName();
                    refColumnN = referencedTablesAndColumns[0].getReferencedColumnName();

                    wpM = work;
                    wpN = mainWork;

                } else {
                    columnM = referencedTablesAndColumns[0].getColumnName();
                    columnN = referencedTablesAndColumns[1].getColumnName();
                    refColumnM = referencedTablesAndColumns[0].getReferencedColumnName();
                    refColumnN = referencedTablesAndColumns[1].getReferencedColumnName();

                    wpM = mainWork;
                    wpN = work;
                }
                String connectNtoMRows = DatabaseOperations.connectNtoMRows(wpM, wpN, refColumnM, refColumnN, dc, mToNTable, columnM, columnN);
                work.getStateLabel().setText("Connection made! " + connectNtoMRows);
                mainWork.getStateLabel().setText("Connection made!");
                displayDependingResults();

            } else {

                Object value = work.getCellFromSelected(columnHoldingKeyValue);
                DatabaseOperations.update(mainWork.getResultTable(), mainTableName, dc, mainTable, value, keyToSearchFor);
                work.getStateLabel().setText("Connection made!");
                mainWork.getStateLabel().setText("Connection made! Changed to value: " + value);
                displayDependingResults();
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(parent, ex + "\nConnection could not be made!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Remove a connection between description and main table.
     */
    private void removeConnection() {
        try {

            setPanelToWorkWith();
            if (mToNTable != null) {

                Table descriptionTable = dc.getColumnInfo(mToNTable);
                DatabaseOperations.delete(dependentResultTable, mToNTable, dc,
                        descriptionTable, dependentResultTableModel);
            } else {

                int option = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this?\nIt cannot be undone!",
                        "Deleting entries...", JOptionPane.YES_NO_OPTION);

                if (option == JOptionPane.YES_OPTION) {
                    DatabaseOperations.update(dependentResultTable, mainTableName, dc, mainTable, null, keyToSearchFor);
                    displayDependingResults();
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(parent, ex + "\nConnection could not be deleted!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Opens a dialog which lets the user edit additional description columns in
     * a m:n-table.
     */
    private void extraMtoNColumn() {

        try {
            if (dependentResultTable.getSelectedRow() != -1) {

                ForeignKeyColumn[] rtacns = dc.getReferencedTableAndColumnNames(mToNTable);
                Object cellM = DatabaseOperations.getCellFromSelected(rtacns[0].getReferencedColumnName(), dependentResultTable, dependentResultTableModel);
                Object cellN = DatabaseOperations.getCellFromSelected((String) rtacns[1].getReferencedColumnName(), dependentResultTable, dependentResultTableModel);

                ExtraMtoNDescriptionDialog emnd = new ExtraMtoNDescriptionDialog(parent, true,
                        extraMtoNdescriptionColumns, dc, mToNTable, rtacns[0].getColumnName(), rtacns[1].getColumnName(),
                        cellM, cellN);
                displayDependingResults();
            } else {

                JOptionPane.showMessageDialog(parent, "Select at least one row!",
                        "No selection made", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(parent, ex + "\nExtra column could not be loaded!", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Shows the selected row from the dependent result table in the description
     * result table. The displayed row is automatically selected (This part
     * should be reviewed...).
     */
    private void toDescription() {

        setPanelToWorkWith();
        int selectedRow = dependentResultTable.getSelectedRow();
        if (selectedRow != -1) {
            try {
                Object descriptionTableId = DatabaseOperations.getCellFromSelected(columnHoldingKeyValue,
                        dependentResultTable, dependentResultTableModel);
                ResultSet descriptionTableEntry = dc.search(descriptionTableName, columnHoldingKeyValue, descriptionTableId, null);
                int rowCount = work.display(descriptionTableEntry);
                if (rowCount == 1) {

                    work.getResultTable().selectAll();
                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(parent, ex + "\nCould not be loaded!", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {

            JOptionPane.showMessageDialog(parent, "Select at least one row!",
                    "No selection made", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void allToDescription() {

//        setPanelToWorkWith();
        try {

            for (int i = 0; i < descriptionWorks.length; i++) {

                Object cellFromSelected = DatabaseOperations.getCellFromSelected(mainTableId,
                        mainWork.getResultTable(), mainWork.getResultTableModel());
                ResultSet res;
                if (mToNTables[i] == null) {

                    res = dc.search(mainTableName, mainTableId, cellFromSelected, keyColumnsForSearch[i]);
                } else {

                    referencedTablesAndColumns = dc.getReferencedTableAndColumnNames(mToNTables[i]);
                    res = dc.search(mToNTables[i], referencedTablesAndColumns[0].getColumnName(), cellFromSelected,
                            referencedTablesAndColumns[1].getColumnName());
                }

                res.last();
                int rowCount = res.getRow();

                res.beforeFirst();
                Object[] rows = new Object[rowCount];
                for (int j = 0; res.next(); j++) {

                    rows[j] = res.getObject(1);
                }

                if (rows.length > 0) {

                    ResultSet search = dc.search(descriptionTables[i], descriptionTableIds[i], rows);
                    descriptionWorks[i].display(search);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DictionaryPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainSplitPane = new javax.swing.JSplitPane();
        resultSplitPane = new javax.swing.JSplitPane();
        wordScrollPane = new javax.swing.JScrollPane();
        dependentResultPanel = new javax.swing.JPanel();
        dependentResultScrollPane = new javax.swing.JScrollPane();
        dependentResultTable = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        deleteConnectionButton = new javax.swing.JButton();
        toDescriptionButton = new javax.swing.JButton();
        allToDescriptionButton = new javax.swing.JButton();
        extraColumnButton = new javax.swing.JButton();
        editSplitPane = new javax.swing.JSplitPane();
        buttonPanel = new javax.swing.JPanel();
        deleteButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        insertButton = new javax.swing.JButton();
        searchForButton = new javax.swing.JButton();
        connectButton = new javax.swing.JButton();
        toOrFromMainToggleButton = new javax.swing.JToggleButton();
        searchForEmptyButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        combinedCheckBox = new javax.swing.JCheckBox();
        fromPanelCheckBox = new javax.swing.JCheckBox();
        tabbedPane = new javax.swing.JTabbedPane();

        setPreferredSize(new java.awt.Dimension(500, 500));

        mainSplitPane.setBackground(new java.awt.Color(1, 180, 25));
        mainSplitPane.setDividerLocation(300);

        resultSplitPane.setBackground(new java.awt.Color(1, 180, 25));
        resultSplitPane.setDividerLocation(150);
        resultSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        resultSplitPane.setLeftComponent(wordScrollPane);

        dependentResultTable.setModel(dependentResultTableModel);
        dependentResultTable.getTableHeader().setReorderingAllowed(false);
        dependentResultScrollPane.setViewportView(dependentResultTable);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        deleteConnectionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/components/Set_null.png"))); // NOI18N
        deleteConnectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteConnectionButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(deleteConnectionButton);

        toDescriptionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/containers/To_description.png"))); // NOI18N
        toDescriptionButton.setToolTipText("");
        toDescriptionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toDescriptionButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(toDescriptionButton);

        allToDescriptionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/containers/All_to_description.png"))); // NOI18N
        allToDescriptionButton.setFocusable(false);
        allToDescriptionButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        allToDescriptionButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        allToDescriptionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allToDescriptionButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(allToDescriptionButton);

        extraColumnButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/containers/Additional.png"))); // NOI18N
        extraColumnButton.setEnabled(false);
        extraColumnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                extraColumnButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(extraColumnButton);

        javax.swing.GroupLayout dependentResultPanelLayout = new javax.swing.GroupLayout(dependentResultPanel);
        dependentResultPanel.setLayout(dependentResultPanelLayout);
        dependentResultPanelLayout.setHorizontalGroup(
            dependentResultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dependentResultScrollPane)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dependentResultPanelLayout.setVerticalGroup(
            dependentResultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dependentResultPanelLayout.createSequentialGroup()
                .addComponent(dependentResultScrollPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        resultSplitPane.setRightComponent(dependentResultPanel);

        mainSplitPane.setLeftComponent(resultSplitPane);

        editSplitPane.setBackground(new java.awt.Color(1, 180, 25));
        editSplitPane.setDividerLocation(300);
        editSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        buttonPanel.setBackground(new java.awt.Color(128, 24, 24));

        deleteButton.setText("Delete Selected");
        deleteButton.setPreferredSize(null);
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        updateButton.setText("Update Selected");
        updateButton.setPreferredSize(null);
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        insertButton.setText("Insert");
        insertButton.setPreferredSize(null);
        insertButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                insertButtonActionPerformed(evt);
            }
        });

        searchForButton.setText("Search For...");
        searchForButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchForButtonActionPerformed(evt);
            }
        });

        connectButton.setText("Connect with...");
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });

        toOrFromMainToggleButton.setText("To Main from...");
        toOrFromMainToggleButton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                toOrFromMainToggleButtonItemStateChanged(evt);
            }
        });

        searchForEmptyButton.setText("Search Empty");
        searchForEmptyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchForEmptyButtonActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(128, 24, 24));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        combinedCheckBox.setForeground(new java.awt.Color(254, 254, 254));
        combinedCheckBox.setText("Combined");
        combinedCheckBox.setToolTipText("Combine search parameters from main and description table");

        fromPanelCheckBox.setForeground(new java.awt.Color(254, 254, 254));
        fromPanelCheckBox.setText("From Panel");
        fromPanelCheckBox.setToolTipText("Select for directly searching by search panels of the description table");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fromPanelCheckBox)
                    .addComponent(combinedCheckBox))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fromPanelCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(combinedCheckBox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout buttonPanelLayout = new javax.swing.GroupLayout(buttonPanel);
        buttonPanel.setLayout(buttonPanelLayout);
        buttonPanelLayout.setHorizontalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jSeparator1)
                    .addGroup(buttonPanelLayout.createSequentialGroup()
                        .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(insertButton, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(searchForEmptyButton, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                            .addComponent(searchForButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(buttonPanelLayout.createSequentialGroup()
                                .addComponent(updateButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(buttonPanelLayout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(toOrFromMainToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(connectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(44, 44, 44)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        buttonPanelLayout.setVerticalGroup(
            buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(insertButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(updateButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 153, Short.MAX_VALUE)
                .addGroup(buttonPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, buttonPanelLayout.createSequentialGroup()
                        .addComponent(searchForEmptyButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(searchForButton))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(buttonPanelLayout.createSequentialGroup()
                        .addComponent(toOrFromMainToggleButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(connectButton)))
                .addContainerGap())
        );

        editSplitPane.setTopComponent(buttonPanel);

        tabbedPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabbedPaneStateChanged(evt);
            }
        });
        editSplitPane.setRightComponent(tabbedPane);

        mainSplitPane.setRightComponent(editSplitPane);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainSplitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 835, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainSplitPane)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tabbedPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabbedPaneStateChanged
        // TODO add your handling code here:
        displayDependingResults();
    }//GEN-LAST:event_tabbedPaneStateChanged

    private void insertButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_insertButtonActionPerformed
        // TODO add your handling code here:
        insertInMain();
    }//GEN-LAST:event_insertButtonActionPerformed

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        // TODO add your handling code here:
        updateMain();
    }//GEN-LAST:event_updateButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        // TODO add your handling code here:
        deleteFromMain();
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void searchForButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchForButtonActionPerformed
        // TODO add your handling code here:
        searchFor();
    }//GEN-LAST:event_searchForButtonActionPerformed

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectButtonActionPerformed
        // TODO add your handling code here:
        connectWith();
    }//GEN-LAST:event_connectButtonActionPerformed

    private void toOrFromMainToggleButtonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_toOrFromMainToggleButtonItemStateChanged
        // TODO add your handling code here:
        changeToggleButton();
    }//GEN-LAST:event_toOrFromMainToggleButtonItemStateChanged

    private void deleteConnectionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteConnectionButtonActionPerformed
        // TODO add your handling code here:
        removeConnection();
    }//GEN-LAST:event_deleteConnectionButtonActionPerformed

    private void extraColumnButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_extraColumnButtonActionPerformed
        // TODO add your handling code here:
        extraMtoNColumn();
    }//GEN-LAST:event_extraColumnButtonActionPerformed

    private void searchForEmptyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchForEmptyButtonActionPerformed
        // TODO add your handling code here:
        searchForEmpty();
    }//GEN-LAST:event_searchForEmptyButtonActionPerformed

    private void toDescriptionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toDescriptionButtonActionPerformed
        // TODO add your handling code here:
        toDescription();
    }//GEN-LAST:event_toDescriptionButtonActionPerformed

    private void allToDescriptionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allToDescriptionButtonActionPerformed
        // TODO add your handling code here:
        allToDescription();
    }//GEN-LAST:event_allToDescriptionButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton allToDescriptionButton;
    private javax.swing.JPanel buttonPanel;
    private javax.swing.JCheckBox combinedCheckBox;
    private javax.swing.JButton connectButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JButton deleteConnectionButton;
    private javax.swing.JPanel dependentResultPanel;
    private javax.swing.JScrollPane dependentResultScrollPane;
    private javax.swing.JTable dependentResultTable;
    private javax.swing.JSplitPane editSplitPane;
    private javax.swing.JButton extraColumnButton;
    private javax.swing.JCheckBox fromPanelCheckBox;
    private javax.swing.JButton insertButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JSplitPane mainSplitPane;
    private javax.swing.JSplitPane resultSplitPane;
    private javax.swing.JButton searchForButton;
    private javax.swing.JButton searchForEmptyButton;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JButton toDescriptionButton;
    private javax.swing.JToggleButton toOrFromMainToggleButton;
    private javax.swing.JButton updateButton;
    private javax.swing.JScrollPane wordScrollPane;
    // End of variables declaration//GEN-END:variables
}
