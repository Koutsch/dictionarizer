/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package containers;

import components.WorkingPanel;
import dialogs.DatabaseConnectionDialog;
import beans.AccessData;
import dialogs.TypeLister;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import main.DatabaseConnector;
import main.XmlCreator;

/**
 * The MainFrame is the basic container. From here the whole program starts.
 *
 * @author koutsch
 */
public class MainFrame extends javax.swing.JFrame {

    AccessData data = new AccessData();
    DatabaseConnector dc;
    private String catalog;

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        boolean connected = connectToDatabase();
        if (connected) {

            setVisible(true);
            this.setTitle("Dictionarizer on: '" + catalog + "'");
            initComponents();
        } else {
            this.dispose();
        }
    }

    /**
     * Opens a Dialog which prompts the user for the database connection
     * informations. After that it connects to the database.
     *
     * @return true if a connection has been established successfully
     */
    private boolean connectToDatabase() {

        DatabaseConnectionDialog dcd = new DatabaseConnectionDialog(data, this, true);
        if (data.getUrl() != null) {
            dc = new DatabaseConnector();
            try {
                catalog = dc.connect(data.getUrl(), data.getUsername(), String.valueOf(data.getPassword()));
                return true;

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Cannot start because "
                        + "cannot connect to database!\nReason: " + ex);
                return false;
            }
        }
        return false;
    }

    /**
     * Asks the user if really wants to close the current work mode.
     *
     * @return true if yes, false if not
     */
    private boolean closeDialog() {

        int cCnt = scrollPane.getViewport().getComponentCount();
        if (cCnt != 0) {
            int dec = JOptionPane.showConfirmDialog(this, "Current mode will be closed! Proceed?",
                    "Open New Mode", JOptionPane.OK_CANCEL_OPTION);
            if (dec == JOptionPane.CANCEL_OPTION) {

                return false;
            }
        }
        return true;
    }

    /**
     * Displays a text field where the user can put SQL statement directly.
     * Every statement has to be separated by ';'. If one execution fails,
     * everything is rolled back.
     */
    private void directSQLMode() {
        try {

            if (closeDialog()) {

                dc.setCommitMode(false);

                scrollPane.getViewport().removeAll();
                scrollPane.validate();

                WorkingPanel w = new WorkingPanel(dc, null);
                w.addSqlTextArea();
                w.executeSQLButton();
                w.resultToFileButton();

                scrollPane.setViewportView(w);

            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Shows the "table mode" where one database table can be accessed. Every
     * column gets its own ColumnPanel.
     */
    private void tableMode() {
        try {
            Object[] ts = dc.getTableNames();
            Object cT = JOptionPane.showInputDialog(this, "Choose a table:",
                    "Tables", JOptionPane.QUESTION_MESSAGE, null, ts, null);

            if (cT != null) {

                dc.setCommitMode(true);

                scrollPane.getViewport().removeAll();
                scrollPane.validate();

                WorkingPanel w = new WorkingPanel(dc, null);
                w.setTableName((String) cT);
                w.populate();
                w.tableModeButtons();
                scrollPane.setViewportView(w);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Opens the "dictionary mode" where the main table is presented left and
     * the related description tables are presented in a tabbed pane on the
     * right. To work with this mode first a description XML-file has to be
     * chosen.
     */
    private void dictionaryMode() {
        try {

            javax.swing.JFileChooser fc = new javax.swing.JFileChooser();
            fc.setFileFilter(new FileNameExtensionFilter("xml", "xml"));
            int showOpenDialog = fc.showOpenDialog(this);

            if (showOpenDialog == JFileChooser.APPROVE_OPTION) {

                java.io.File file = fc.getSelectedFile();
                String absolutePath = file.getAbsolutePath();

                dc.setCommitMode(true);

                scrollPane.getViewport().removeAll();
                scrollPane.validate();

                DictionaryPanel dp = new DictionaryPanel(this, dc, absolutePath);

                scrollPane.setViewportView(dp);
            }
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(this, ex, "Error setting up Dictionary Mode", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrollPane = new javax.swing.JScrollPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        modeMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        tableModeMenuItem = new javax.swing.JMenuItem();
        oneToNModeMenuItem = new javax.swing.JMenuItem();
        toolMenu = new javax.swing.JMenu();
        writeXmlMenuItem = new javax.swing.JMenuItem();
        typeListerMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        helpMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        modeMenu.setText("Modes");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Dictionary Mode");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        modeMenu.add(jMenuItem1);

        tableModeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        tableModeMenuItem.setText("Table Mode");
        tableModeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tableModeMenuItemActionPerformed(evt);
            }
        });
        modeMenu.add(tableModeMenuItem);

        oneToNModeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        oneToNModeMenuItem.setText("SQL Mode");
        oneToNModeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oneToNModeMenuItemActionPerformed(evt);
            }
        });
        modeMenu.add(oneToNModeMenuItem);

        jMenuBar1.add(modeMenu);

        toolMenu.setText("Tools");

        writeXmlMenuItem.setText("Write new dictionary XML...");
        writeXmlMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                writeXmlMenuItemActionPerformed(evt);
            }
        });
        toolMenu.add(writeXmlMenuItem);

        typeListerMenuItem.setText("Type Lister...");
        typeListerMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeListerMenuItemActionPerformed(evt);
            }
        });
        toolMenu.add(typeListerMenuItem);

        jMenuBar1.add(toolMenu);

        helpMenu.setText("Help");

        helpMenuItem.setText("Help...");
        helpMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(helpMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 889, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 712, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableModeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tableModeMenuItemActionPerformed
        // TODO add your handling code here:
        tableMode();
    }//GEN-LAST:event_tableModeMenuItemActionPerformed

    private void oneToNModeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oneToNModeMenuItemActionPerformed
        // TODO add your handling code here:
        directSQLMode();
    }//GEN-LAST:event_oneToNModeMenuItemActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        dictionaryMode();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void writeXmlMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_writeXmlMenuItemActionPerformed
        try {
            // TODO add your handling code here:
            XmlCreator xc = new XmlCreator(this);
            xc.save();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, ex, "Error writing file", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_writeXmlMenuItemActionPerformed

    private void helpMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpMenuItemActionPerformed
        // TODO add your handling code here:
        HelpContainer hc = new HelpContainer();
    }//GEN-LAST:event_helpMenuItemActionPerformed

    private void typeListerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeListerMenuItemActionPerformed
        // TODO add your handling code here:
        TypeLister tl = new TypeLister(this, dc, true);
    }//GEN-LAST:event_typeListerMenuItemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame mainFrame = new MainFrame();
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenuItem helpMenuItem;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenu modeMenu;
    private javax.swing.JMenuItem oneToNModeMenuItem;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JMenuItem tableModeMenuItem;
    private javax.swing.JMenu toolMenu;
    private javax.swing.JMenuItem typeListerMenuItem;
    private javax.swing.JMenuItem writeXmlMenuItem;
    // End of variables declaration//GEN-END:variables
}
