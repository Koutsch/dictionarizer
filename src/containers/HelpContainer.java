package containers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 *
 * @author jawiedne
 */
public class HelpContainer extends javax.swing.JFrame {

    /**
     * Creates new form HelpContainer
     */
    public HelpContainer() {

        initComponents();
        helpText();
        setVisible(true);
    }

    private void helpText() {

        helpEditorPane.setEditable(false);
        helpEditorPane.setContentType("text/html");
        try {
            java.net.URL helpURL = HelpContainer.class.getResource("helpText.html");
            java.net.URL aboutURL = HelpContainer.class.getResource("aboutText.html");
            helpEditorPane.setPage(helpURL);
            aboutEditorPane.setPage(aboutURL);
        } catch (MalformedURLException ex) {
            helpEditorPane.setText("No help contents found!\n" + ex);
            aboutEditorPane.setText("No about contents found!\n" + ex);
        } catch (IOException ex) {
            helpEditorPane.setText("No help contents found!\n" + ex);
            aboutEditorPane.setText("No about contents found!\n" + ex);
        }
        helpEditorPane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    
                    URL url = e.getURL();
                    String ref = url.getRef();
                    helpEditorPane.scrollToReference(ref);
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPane = new javax.swing.JTabbedPane();
        helpScrollPane = new javax.swing.JScrollPane();
        helpEditorPane = new javax.swing.JEditorPane();
        aboutPanel = new javax.swing.JPanel();
        versionLabel = new javax.swing.JLabel();
        aboutScrollPane = new javax.swing.JScrollPane();
        aboutEditorPane = new javax.swing.JEditorPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("HELP");

        helpEditorPane.setEditable(false);
        helpScrollPane.setViewportView(helpEditorPane);

        tabbedPane.addTab("Help", helpScrollPane);

        versionLabel.setText("Dictionarizer - Version 0.4.0");

        aboutScrollPane.setViewportView(aboutEditorPane);

        javax.swing.GroupLayout aboutPanelLayout = new javax.swing.GroupLayout(aboutPanel);
        aboutPanel.setLayout(aboutPanelLayout);
        aboutPanelLayout.setHorizontalGroup(
            aboutPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(versionLabel)
                .addContainerGap(188, Short.MAX_VALUE))
            .addComponent(aboutScrollPane)
        );
        aboutPanelLayout.setVerticalGroup(
            aboutPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(versionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(aboutScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE))
        );

        tabbedPane.addTab("About", aboutPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPane)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane aboutEditorPane;
    private javax.swing.JPanel aboutPanel;
    private javax.swing.JScrollPane aboutScrollPane;
    private javax.swing.JEditorPane helpEditorPane;
    private javax.swing.JScrollPane helpScrollPane;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JLabel versionLabel;
    // End of variables declaration//GEN-END:variables
}
