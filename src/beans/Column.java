/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author koutsch
 */
public class Column {

    private final String name;
    private final boolean primaryKey;
    private boolean autoIncrement = false;

    /**
     *
     */
    public Column() {

        this.name = null;
        this.primaryKey = false;
    }

    /**
     * Create a new Column object.
     *
     * @param name the column name
     * @param primaryKey true if primary key, false if not
     */
    public Column(String name, boolean primaryKey) {

        this.name = name;
        this.primaryKey = primaryKey;
    }

    /**
     * Determine if a column is a primary key.
     *
     * @return true if column is primary key, false if not
     */
    public boolean isPrimaryKey() {
        return primaryKey;
    }

    /**
     * Get the name of the column.
     *
     * @return the name as String
     */
    public String getName() {
        return name;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }
}
