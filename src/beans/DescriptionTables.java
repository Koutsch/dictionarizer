package beans;

/**
 *
 * @author jawiedne
 */
public class DescriptionTables {
    
    private final String[][] descriptionTableData;
    private final String[][] extradescriptionColumnData;

    public DescriptionTables(String[][] descriptionTableData, String[][] extradescriptionColumnData) {
        this.descriptionTableData = descriptionTableData;
        this.extradescriptionColumnData = extradescriptionColumnData;
    }

    public String[][] getDescriptionTableData() {
        return descriptionTableData;
    }

    public String[][] getExtradescriptionColumnData() {
        return extradescriptionColumnData;
    }
    
    
}
