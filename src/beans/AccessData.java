package beans;

import java.io.Serializable;

/**
 *
 * @author koutsch
 */
public class AccessData implements Serializable {

    String url;
    String username;
    char[] password;

    /**
     *
     */
    public AccessData() {

        this.url = null;
        this.username = null;
        this.password = null;
    }

    /**
     * Get the database url.
     * 
     * @return the database url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Get the database username.
     *
     * @return the database username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the database user password.
     *
     * @return the password as char[]
     */
    public char[] getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(char[] password) {
        this.password = password;
    }
}
