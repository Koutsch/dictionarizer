package beans;

import java.util.ArrayList;

/**
 *
 * @author jawiedne
 */
public class Table {

    private final ArrayList<Column> columns;

    /**
     *
     */
    public Table() {
        this.columns = new ArrayList<>();
    }

    public void addColumn(Column c) {

        columns.add(c);
    }

    public Column[] getColumns() {

        Column[] colArr = new Column[columnCount()];
        return columns.toArray(colArr);
    }

    public int columnCount() {

        return columns.size();
    }

    public int[] getPrimaryKeyIndices() {

        ArrayList<Integer> pki = new ArrayList<>();
        for (int i = 0; i < columnCount(); i++) {

            if (columns.get(i).isPrimaryKey()) {

                pki.add(i);
            }
        }
        int[] pkiArr = new int[pki.size()];
        for (int i = 0; i < pkiArr.length; i++) {
            pkiArr[i] = pki.get(i);
        }
        return pkiArr;
    }

    public String[] getColumnNames() {

        String[] columnNames = new String[columnCount()];
        for (int i = 0; i < columnCount(); i++) {

            columnNames[i] = columns.get(i).getName();
        }
        return columnNames;
    }

    /**
     * Returns the name of the auto increment column.
     * 
     * @return the column name or null if there's no such column
     */
    public String getAutoIncrementColumnName() {

        String autoIncrementColumnName = null;
        for (int i = 0; i < columnCount(); i++) {

            if (columns.get(i).isAutoIncrement()) {

                autoIncrementColumnName = columns.get(i).getName();
            }
        }
        return autoIncrementColumnName;
    }
}
