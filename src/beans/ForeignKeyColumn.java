package beans;

/**
 *
 * @author jawiedne
 */
public class ForeignKeyColumn {

    private final String columnName;
    private final String referencedColumnName;
    private final String referencedTableName;

    public ForeignKeyColumn(String columnName, String referencedColumnName, String referencedTableName) {
        this.columnName = columnName;
        this.referencedColumnName = referencedColumnName;
        this.referencedTableName = referencedTableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getReferencedColumnName() {
        return referencedColumnName;
    }

    public String getReferencedTableName() {
        return referencedTableName;
    }
}
