package main;

import beans.Column;
import beans.ForeignKeyColumn;
import beans.Table;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The DatabaseConnector handles all direct communication with the database.
 * Currently supported database management systems: SQLite, PostgreSQL, MySQL
 *
 * @author Jakob Wiedner
 */
public class DatabaseConnector {

    Connection con;
    public final static int sqlite = 0;
    public final static int postgresql = 1;
    public final static int mysql = 2;
    public final static String postgresqlName = "PostgreSQL";
    public final static String sqliteName = "SQLite";
    public final static String mysqlName = "MySQL";
    int dbms;
    public final static int STARTS_WITH = 0;
    public final static int ENDS_WITH = 1;
    public final static int CONTAINS = 2;
    public final static int EXACT = 3;
    /**
     * Unused.
     */
    public final static String getSchemaNamesPostgreSQL = "select schema_name from information_schema.schemata where "
            + "schema_name <> 'information_schema' and "
            + "schema_name <> 'public' and schema_name !~ E'^pg_'";
    public final static String getSqliteTableNames = "SELECT name FROM sqlite_master WHERE type='table';";
    public final static String getPostgresqlTableNames = "SELECT table_schema"
            + " || '.' || table_name FROM information_schema.tables"
            + "WHERE table_type = 'BASE TABLE' "
            + "AND table_schema NOT IN ('pg_catalog', 'information_schema');";
    public final static String getMysqlTableNames = "SHOW TABLES";

    /**
     * Create a new DatabaseConnector.
     */
    public DatabaseConnector() {

        con = null;
    }

    /**
     * Connect to a database.
     *
     * @param url the database url (Note that it's necessary to write "jdbc:")
     * @param usr the user name
     * @param pw the password to access the database
     * @return the connection's catalog name
     *
     * @throws SQLException
     */
    public String connect(String url, String usr, String pw) throws SQLException {

        con = DriverManager.getConnection("jdbc:" + url, usr, pw);
        setDbms();
        return con.getCatalog();
    }

    /**
     * Get the name of the database management system the database is connected
     * to.
     *
     * @return a String representing the name of the database management system
     * @throws SQLException
     */
    public String getDbmsName() throws SQLException {

        DatabaseMetaData dmd = con.getMetaData();
        return dmd.getDatabaseProductName();
    }

    /**
     * Returns an integer representing the database management systems supported
     * by Dictionarizer.
     *
     * @return 0: SQLite, 1: PostgreSQL, 2: MySQL
     */
    public int getDbms() {

        return dbms;
    }

    /**
     * Reads the database metadata and determines which database management
     * system (from the supported ones) is to use.
     *
     * @throws SQLException
     */
    public void setDbms() throws SQLException {

        DatabaseMetaData dmd = con.getMetaData();
        String dbmsName = dmd.getDatabaseProductName();

        if (dbmsName.equals(sqliteName)) {

            dbms = sqlite;
        }

        if (dbmsName.equals(postgresqlName)) {

            dbms = postgresql;
        }

        if (dbmsName.equals(mysqlName)) {

            dbms = mysql;
        }
    }

    /**
     * Set the database connection commit mode.
     *
     * @param aC true: auto commit is enabled, false: auto commit is disabled
     * @throws SQLException
     */
    public void setCommitMode(boolean aC) throws SQLException {

        con.setAutoCommit(aC);
    }

    /**
     * Executes an SQL statement. Auto commit must be disabled!
     *
     * @param sql the SQL String to execute
     * @return a ResultSet if SQL was a query, null otherwise
     * @throws SQLException
     */
    public ResultSet execute(String sql) throws SQLException {

        Statement state = con.createStatement();
        ResultSet res;
        boolean query = state.execute(sql);
        if (query) {

            res = state.getResultSet();
        } else {

            res = null;
        }
        return res;
    }

    /**
     * Commits the SQL statements executed. Auto commit must be disabled!
     *
     * @throws SQLException
     */
    public void commit() throws SQLException {

        con.commit();
    }

    /**
     * Undoes executed SQL statements. Auto commit must be disabled!
     *
     * @throws SQLException
     */
    public void rollback() throws SQLException {

        con.rollback();
    }

    /**
     * Get all rows from a ResultSet column.
     *
     * @param res the ResultSet to look through
     * @param cI the index of the column to go through (starts with 1!)
     * @return an Object[] holding all row values from the selected column
     * @throws SQLException
     */
    private Object[] getAllRows(ResultSet res, int cI) throws SQLException {

        ArrayList<Object> list = new ArrayList<>();
        for (; res.next();) {

            list.add(res.getString(cI));
        }
        Object[] rowValues = list.toArray(new Object[list.size()]);
        return rowValues;
    }

    /**
     * Gets the names of all (non-system) tables in a database.
     *
     * @return an Object[] with String representations of the table names
     * @throws SQLException
     */
    public Object[] getTableNames() throws SQLException {

        Statement state = con.createStatement();
        ResultSet res;

        String sql;
        switch (dbms) {

            case 0:
                sql = getSqliteTableNames;
                break;
            case 1:
                sql = getPostgresqlTableNames;
                break;
            case 2:
                sql = getMysqlTableNames;
                break;
            default:
                sql = "";
                break;
        }

        res = state.executeQuery(sql);
        return getAllRows(res, 1);
    }

    /**
     * Get all column names and a corresponding boolean determining if a column
     * is a primary key. The auto increment function is currently only available
     * for MySql.
     *
     * @param t the name of the table to look for
     * @return a Table object containing information about all columns
     * @throws SQLException what to write...
     */
    public Table getColumnInfo(String t) throws SQLException {

        Statement state = con.createStatement();
        ResultSet res;
        Table table = new Table();

        switch (dbms) {

            case 0:
                res = state.executeQuery("PRAGMA table_info(" + t + ");");
                for (; res.next();) {

                    Column column = new Column(res.getString(2), res.getBoolean(6));
                    table.addColumn(column);
                }
                break;
            case 1:
                res = state.executeQuery("SELECT a.attname, "
                        + "coalesce(p.indisprimary, FALSE) "
                        + "FROM pg_attribute a LEFT JOIN pg_index p "
                        + "ON p.indrelid = a.attrelid AND a.attnum = ANY(p.indkey) "
                        + "WHERE a.attnum > 0 AND NOT a.attisdropped "
                        + "AND a.attrelid = '" + t + "'::regclass;");
                for (; res.next();) {

                    Column column = new Column(res.getString(1), res.getBoolean(2));
                    table.addColumn(column);
                }
                break;
            case 2:
                res = state.executeQuery("SHOW columns FROM " + t + ";");
                for (; res.next();) {

                    String key = res.getString(4);
                    String autoIncrement = res.getString(6);
                    Column column = new Column(res.getString(1), key.equals("PRI"));
                    if (autoIncrement.equals("auto_increment")) {

                        column.setAutoIncrement(true);
                    }
                    table.addColumn(column);
                }
            default:
                break;
        }
        return table;
    }

    /**
     * Get all the column names from a table.
     *
     * @param t the name of the table the columns are in
     * @return a String[] representing the column names
     * @throws SQLException
     */
    public Object[] getColumnNames(String t) throws SQLException {

        Statement state = con.createStatement();
        ResultSet res;

        String sql;
        int resultColumn;
        switch (dbms) {

            case 0:
                sql = "PRAGMA table_info(" + t + ");";
                resultColumn = 2;
                break;
            case 1:
                sql = "SELECT a.attname FROM pg_attribute a "
                        + "WHERE a.attnum > 0 AND NOT a.attisdropped "
                        + "AND a.attrelid = '" + t + "'::regclass;";
                resultColumn = 1;
                break;
            case 2:
                sql = "SHOW columns FROM " + t + ";";
                resultColumn = 1;
                break;
            default:
                sql = "";
                resultColumn = -1;
                break;
        }
        res = state.executeQuery(sql);
        return getAllRows(res, resultColumn);
    }

    /**
     * Creates a SQL clause to use with WHERE to search for multiple column
     * contents.
     *
     * @param cTS the key is the column name, the value is the String to search
     * for
     * @param sM an int determining how to search - 0: starts with, 1: ends
     * with, 2: contains or 3: is exact match
     *
     * @return the SQL clause as String
     */
    private String createSearchClause(HashMap<String, Object> cTS, int sM) {

        StringBuilder sb = new StringBuilder();
        if (!cTS.isEmpty()) {

            int count = 0;
            for (String key : cTS.keySet()) {

                switch (sM) {

                    case 0:
                        sb.append(key).append(" LIKE '").append(cTS.get(key)).append("%'");
                        break;
                    case 1:
                        sb.append(key).append(" LIKE '%").append(cTS.get(key)).append("'");
                        break;
                    case 2:
                        sb.append(key).append(" LIKE '%").append(cTS.get(key)).append("%'");
                        break;
                    case 3:
                        sb.append(key).append(" = '").append(cTS.get(key)).append("'");
                        break;
                    default:
                        sb.append(key).append(" = '").append(cTS.get(key)).append("'");
                        break;
                }
                count++;
                if (count < cTS.size()) {

                    sb.append(" AND ");
                }
            }
        }
        return sb.toString();
    }

    /**
     * Performs a search. The cool stuff in it is the "SEARCHO MULTIPLO".
     *
     * @param t the name of the table to look through
     * @param cTS the key is the column name, the value is the String to search
     * for
     * @param sM an int determining how to search - 0: starts with, 1: ends
     * with, 2: contains or 3: is exact match
     * @param oBC the name of the column to be ordered by
     * @param rC the name of the column to retrieve results from; if null all
     * columns are returned
     *
     * @return a ResultSet showing all matching rows
     *
     * @throws SQLException
     */
    public ResultSet search(String t, HashMap<String, Object> cTS, int sM, String oBC, String rC) throws SQLException {

        if (rC == null) {

            rC = "*";
        }
        StringBuilder sb = new StringBuilder("SELECT " + rC + " FROM " + t);
        Statement state = con.createStatement();
        ResultSet res;

        if (!cTS.isEmpty()) {

            sb.append(" WHERE ");
            int count = 0;
            for (String key : cTS.keySet()) {

                switch (sM) {

                    case 0:
                        sb.append(key).append(" LIKE '").append(cTS.get(key)).append("%'");
                        break;
                    case 1:
                        sb.append(key).append(" LIKE '%").append(cTS.get(key)).append("'");
                        break;
                    case 2:
                        sb.append(key).append(" LIKE '%").append(cTS.get(key)).append("%'");
                        break;
                    case 3:
                        sb.append(key).append(" = '").append(cTS.get(key)).append("'");
                        break;
                    default:
                        sb.append(key).append(" = '").append(cTS.get(key)).append("'");
                        break;
                }
                count++;
                if (count < cTS.size()) {

                    sb.append(" AND ");
                }
            }
        }

        if (oBC != null) {

            sb.append(" ORDER BY ").append(oBC);
        }
        sb.append(";");
        res = state.executeQuery(sb.toString());
        return res;
    }

    /**
     * Searches an exact match of one cell of an column.
     *
     * @param t the name of the table to look through
     * @param c the name of the table to look through
     * @param v the value to look for
     * @param cTS the name of the column that should be retrieved; if null all
     * columns are returned
     *
     * @return a ResultSet showing all matching rows
     *
     * @throws java.sql.SQLException
     */
    public ResultSet search(String t, String c, Object v, String cTS) throws SQLException {

        if (cTS == null) {

            cTS = "*";
        }
        String sql = "SELECT " + cTS + " FROM " + t + " WHERE " + c + " = '" + v + "';";
        Statement state = con.createStatement();
        ResultSet res = state.executeQuery(sql);
        return res;
    }

    /**
     * Searches for all rows matching specified values. 
     *
     * @param t the name of the table to look through
     * @param c the name of the table to look through
     * @param vsTS the values to look for
     * @return a ResultSet showing all matching rows (all columns from the
     * tables are selected)
     * 
     * @throws SQLException
     */
    public ResultSet search(String t, String c, Object[] vsTS) throws SQLException {

        if (vsTS != null) {

            StringBuilder values = new StringBuilder("SELECT * FROM " + t + " WHERE " + c + " IN (");
            int count = 0;
            for (Object valueToSearch : vsTS) {

                values.append("'").append(valueToSearch).append("'");
                count++;
                if (count < vsTS.length) {

                    values.append(", ");
                }
            }
            values.append(");");
            Statement state = con.createStatement();
            ResultSet res = state.executeQuery(values.toString());
            return res;
        }
        return null;
    }

    /**
     * Searches for all duplicate rows (all occurrences) in a table column.
     *
     * @param t the name of the table to look through
     * @param c the name of the column to look through
     *
     * @return a ResultSet showing all rows having duplicates (all columns from
     * the tables are selected)
     *
     * @throws SQLException
     */
    public ResultSet searchForDuplicates(String t, String c) throws SQLException {

        Statement state = con.createStatement();
        String sql = "SELECT * FROM " + t + " WHERE " + c + " IN "
                + "(SELECT " + c + " FROM " + t
                + " group by " + c + " HAVING count(" + c + ") > 1) ORDER BY "
                + c + ";";
        ResultSet res = state.executeQuery(sql);
        return res;
    }

    /**
     * Searches for empty cells in a table column.
     *
     * @param t the name of the table to look through
     * @param c the name of the column to look through
     *
     * @return a ResultSet showing all rows having no value in the specified
     * column
     *
     * @throws SQLException
     */
    public ResultSet searchForEmptyCells(String t, String c) throws SQLException {

        Statement state = con.createStatement();
        String sql = "SELECT * FROM " + t + " WHERE " + c + " IS NULL;";
        ResultSet res = state.executeQuery(sql);
        return res;
    }

    /**
     * Insert values. Gives you "INSERTO MULTIPLO".
     *
     * @param t the table inserting values into
     * @param cTI the key is the column name, the value is the String to insert
     * @return the auto generated key from the insert, -1 if no key was returned
     *
     * @throws SQLException
     */
    public int insert(String t, HashMap<String, String> cTI) throws SQLException {

        if (t != null && !cTI.isEmpty()) {

            StringBuilder sb = new StringBuilder("INSERT INTO " + t);
            try (Statement state = con.createStatement()) {
                sb.append(" (");
                int count = 0;
                for (String key : cTI.keySet()) {

                    sb.append(key);
                    count++;
                    if (count < cTI.size()) {

                        sb.append(", ");
                    }
                }
                count = 0;
                sb.append(") VALUES (");
                for (String key : cTI.keySet()) {

                    sb.append("'").append(cTI.get(key)).append("'");
                    count++;
                    if (count < cTI.size()) {

                        sb.append(", ");
                    }
                }
                sb.append(");");

                try {

                    state.executeUpdate(sb.toString(), Statement.RETURN_GENERATED_KEYS);
                    ResultSet res = state.getGeneratedKeys();
                    if (res.next()) {
                        int key = res.getInt(1);
                        return key;
                    }
                    return -1;

                } catch (SQLFeatureNotSupportedException ex) {

                    state.executeUpdate(sb.toString());
                    return -1;
                }
            }
        }
        return -1;
    }

    /**
     * Prepares a WHERE-clause with multiple values.
     *
     * @param vs the row values for each column
     * @param cs the column names
     * @return a String representing the SQL WHERE-clause
     */
    private String prepareWhereClause(Object[][] vs, String[] cs) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < vs.length; i++) {

            for (int j = 0; j < vs[i].length; j++) {

                sb.append(cs[j]).append(" = '")
                        .append(vs[i][j]).append("'");

                if (j < (vs[i].length - 1)) {

                    sb.append(" AND ");
                }
            }

            if (i < (vs.length) - 1) {

                sb.append(" OR ");
            }
        }
        return sb.toString();
    }

    private StringBuilder prepareUpdateStatement(String t, HashMap<String, Object> csTU) {

        StringBuilder sb = new StringBuilder("UPDATE " + t + " SET ");
        int count = 0;
        for (String key : csTU.keySet()) {

            sb.append(key).append(" = '").append(csTU.get(key)).append("'");
            count++;
            if (count < csTU.size()) {

                sb.append(", ");
            }
        }
        return sb;
    }

    /**
     * Updates cells in rows specified by the user. Gives you "UPDATO MULTIPLO".
     *
     * @param t the name of the table
     * @param pkVs the values of the primary keys (to identify the right row)
     * @param pkCs the names of the columns containing a primary key
     * @param csTU a HashMap containing the column name as key and the new value
     * that shall be put in as the value
     *
     * @throws SQLException
     */
    public void update(String t, Object[][] pkVs, String[] pkCs,
            HashMap<String, Object> csTU) throws SQLException {

        if (!csTU.isEmpty()) {

            Statement state = con.createStatement();

            StringBuilder sb = prepareUpdateStatement(t, csTU);
            sb.append(" WHERE ").append(prepareWhereClause(pkVs, pkCs));
            sb.append(";");

            state.executeUpdate(sb.toString());
        }
    }

    /**
     * Updates extra m:n table columns. Only single row update is possible!
     *
     * @param t name of the m:n table
     * @param mC name of the m column
     * @param nC name of the n column
     * @param mCv value of the m column
     * @param nCv value of the n column
     * @param csTU a HashMap containing the column name as key and the new value
     * that shall be put in as the value
     *
     * @throws java.sql.SQLException
     */
    public void updateMtoNExtraColumn(String t, String mC, String nC, Object mCv, Object nCv,
            HashMap<String, Object> csTU) throws SQLException {

        if (!csTU.isEmpty()) {

            Statement state = con.createStatement();

            StringBuilder sb = prepareUpdateStatement(t, csTU);
            sb.append(" WHERE ").append(mC).append(" = '").append(mCv).append("' AND ")
                    .append(nC).append(" = '").append(nCv).append("';");

            state.executeUpdate(sb.toString());
        }
    }

    /**
     * Sets a cell in a specified column to null.
     *
     * @param t the name of the table
     * @param pkVs
     * @param pkCs
     * @param cTSN
     * @throws SQLException
     */
    public void setCellToNull(String t, Object[][] pkVs, String[] pkCs,
            String cTSN) throws SQLException {

        StringBuilder sb = new StringBuilder("UPDATE " + t + " SET ");
        try (Statement state = con.createStatement()) {

            sb.append(cTSN).append(" = NULL");
            sb.append(" WHERE ").append(prepareWhereClause(pkVs, pkCs));
            sb.append(";");
            state.executeUpdate(sb.toString());
        }
    }

    /**
     * Deletes multiple rows.
     *
     * @param t the table to delete from
     * @param pkVs the primary key values determining the rows to delete
     * @param pkCs the columns holding the primary keys
     *
     * @throws SQLException
     */
    public void delete(String t, Object[][] pkVs, String[] pkCs) throws SQLException {

        // Bug: Cannot delete recently inserted row, must be a search in between! Maybe Sqlite-Bug!!!
        StringBuilder sb = new StringBuilder("DELETE FROM " + t + " WHERE ");
        try (Statement state = con.createStatement()) {
            sb.append(prepareWhereClause(pkVs, pkCs));
            sb.append(";");
            state.executeUpdate(sb.toString());
        }
    }

    /**
     * Gets all types and their count in a table column.
     *
     * @param t the table to look through
     * @param c the column to get the types from
     *
     * @return a ResultSet holding all types of a table column at (1) and their
     * count at (2)
     *
     * @throws SQLException
     */
    public ResultSet getTypes(String t, String c) throws SQLException {

        Statement state = con.createStatement();
        String sql = "SELECT " + c + ", COUNT(" + c + ") FROM " + t
                + " group by " + c + ";";
        ResultSet res = state.executeQuery(sql);
        return res;
    }

    /**
     * Gets all foreign key columns as well as the referenced table and column
     * names.
     *
     * @param rT the name of the referencing table
     *
     * @return a ForeignKeyColumn array holding the key reference information
     *
     * @throws SQLException
     */
    public ForeignKeyColumn[] getReferencedTableAndColumnNames(String rT) throws SQLException {

        Statement state = con.createStatement();
        ResultSet res;

        String sql;
        int refTableNameColumn;
        int refColumnNameColumn;
        int columnName;
        switch (dbms) {

            case 0:
                sql = "";
                refTableNameColumn = 0;
                refColumnNameColumn = 0;
                columnName = 0;
                break;
            case 1:
                sql = "";
                refTableNameColumn = 0;
                refColumnNameColumn = 0;
                columnName = 0;
                break;
            case 2:
                sql = "SELECT referenced_table_name, referenced_column_name, column_name FROM information_schema.KEY_COLUMN_USAGE WHERE "
                        + "referenced_table_name IS NOT NULL AND table_name "
                        + "= '" + rT + "';";
                refTableNameColumn = 1;
                refColumnNameColumn = 2;
                columnName = 3;
                break;
            default:
                sql = "";
                refTableNameColumn = 0;
                refColumnNameColumn = 0;
                columnName = 0;
                break;
        }

        res = state.executeQuery(sql);
        ArrayList<ForeignKeyColumn> list = new ArrayList<>();
        for (; res.next();) {

            ForeignKeyColumn fkc = new ForeignKeyColumn(res.getString(columnName), res.getString(refColumnNameColumn),
                    res.getString(refTableNameColumn));
            list.add(fkc);
        }
        ForeignKeyColumn[] references = list.toArray(new ForeignKeyColumn[list.size()]);
        return references;
    }

    /**
     * Inserts values into a m:n-table.
     *
     * @param t the name of the m:n-table
     * @param cM the name of the m-table column (one or many values)
     * @param cN the name of the n-table column (one value)
     * @param vsM the m-value(s)
     * @param vN the n-value
     *
     * @throws SQLException
     */
    public void insertNtoMConnections(String t, String cM, String cN,
            Object[] vsM, Object vN) throws SQLException {

        Statement state = con.createStatement();
        String sql;
        for (Object valueN : vsM) {

            switch (dbms) {

                case 0:
                    sql = "";
                    break;
                case 1:
                    sql = "";
                    break;
                case 2:
                    sql = "INSERT IGNORE INTO " + t + " (" + cM + ", " + cN
                            + ") VALUES ('" + valueN + "', '" + vN + "');";
                    break;
                default:
                    sql = "";
                    break;
            }
            state.executeUpdate(sql);
        }
    }

    /**
     * Creates a SQL clause to be used in combination with WHERE to search for
     * table results from a table related by a foreign key.
     *
     * @param vsTS the values to search for
     * @param sK the name of key from the table holding the value to search for
     * @param tTS the name of the table holding the value to search for
     * @param cTS the name of the column holding the value to search for
     * @param rK the name of key from the table to show which is related to the
     * table holding the value to search for
     *
     * @return the SQL clause as String
     */
    private String createSearchByRelatedTableClause(Object[] vsTS, String sK,
            String tTS, String cTS, String rK) {

        StringBuilder values = new StringBuilder();
        int count = 0;
        for (Object valueToSearch : vsTS) {

            values.append("'").append(valueToSearch).append("'");
            count++;
            if (count < vsTS.length) {

                values.append(", ");
            }
        }
        return rK + " IN "
                + "(SELECT " + sK + " FROM " + tTS + " WHERE " + cTS + " IN (" + values.toString() + "))";
    }

    /**
     * Searches for all results of one table where the value to search for is
     * located in a foreign key related table. Supports only single key
     * relations!
     *
     * @param tTD the name of the table with the results to show
     * @param rK the name of key from the table to show which is related to the
     * table holding the value to search for
     * @param sK the name of key from the table holding the value to search for
     * @param tTS the name of the table holding the value to search for
     * @param cTS the name of the column holding the value to search for
     * @param vsTS the values to search for
     * @param oBC the name of the column to be ordered by
     *
     * @return a ResultSet showing all matching rows (all columns from the
     * tables are selected)
     *
     * @throws SQLException
     */
    public ResultSet searchByRelatedTable(String tTD, String rK, String sK,
            String tTS, String cTS, Object[] vsTS, String oBC) throws SQLException {

        ResultSet res;
        Statement state = con.createStatement();
        res = state.executeQuery("SELECT * FROM " + tTD + " WHERE "
                + createSearchByRelatedTableClause(vsTS, sK, tTS, cTS, rK) + " ORDER BY " + oBC + ";");
        return res;
    }

    /**
     * Performs a combined search of table search and foreign key related search
     * conditions.
     *
     * @param tTD the name of the table with the results to show
     * @param vsTS the name of key from the table to show which is related to
     * the table holding the value to search for
     * @param sK the name of key from the table holding the value to search for
     * @param tTS the name of the table holding the value to search for
     * @param cTS the name of the column holding the value to search for
     * @param rK the name of the column to be ordered by
     * @param cTSM the key is the column name, the value is the String to search
     * for
     * @param sM an integer determining how to search - 0: starts with, 1: ends
     * with, 2: contains or 3: is exact match
     * @param oBC the name of the column to be ordered by
     *
     * @return a ResultSet showing all matching rows (all columns from the
     * tables are selected)
     * @throws java.sql.SQLException
     */
    public ResultSet combinedSearch(String tTD, Object[] vsTS, String sK, String tTS,
            String cTS, String rK, HashMap<String, Object> cTSM, int sM, Object oBC) throws SQLException {

        StringBuilder sb = new StringBuilder("SELECT * FROM " + tTD + " WHERE ");
        sb.append(createSearchByRelatedTableClause(vsTS, sK, tTS, cTS, rK));
        if (!cTSM.isEmpty()) {

            sb.append(" AND ");
            sb.append(createSearchClause(cTSM, sM));
        }
        sb.append(" ORDER BY ").append(oBC).append(";");

        ResultSet res;
        Statement state = con.createStatement();
        res = state.executeQuery(sb.toString());
        return res;
    }

    /**
     * Searches through a table for rows which are not connected to another
     * table by a m:n-table. Supports only single key relations!
     *
     * @param mTV the main table view
     * @param mTPK the main table primary key
     * @param mnFK the related foreign key of the m:n-table
     * @param mnT the m:n-table
     * @param oBC the name of the column to be ordered by
     * @return a ResultSet with all main table rows without connection
     * @throws SQLException
     */
    public ResultSet searchForNotRelatedTables(String mTV, String mTPK, String mnFK, String mnT, String oBC) throws SQLException {

        ResultSet res;
        Statement state = con.createStatement();
        String sql = "SELECT * FROM " + mTV + " WHERE " + mTPK + " NOT IN (SELECT " + mnFK + " FROM " + mnT + ")"
                + " ORDER BY " + oBC + ";";
        res = state.executeQuery(sql);
        return res;
    }

    /**
     * Searches through a table for rows which are not connected to another
     * table by a o:n-relation. Supports only single key relations!
     *
     * @param mTV the main table view
     * @param mTPK the main table primary key
     * @param mT the main table
     * @param onFK the main table foreign key
     * @param oBC the name of the column to be ordered by
     * @return a ResultSet with all main table rows without connection
     * @throws SQLException
     */
    public ResultSet searchForEmptyFKs(String mTV, String mTPK, String mT, String onFK, String oBC) throws SQLException {

        ResultSet res;
        Statement state = con.createStatement();
        String sql = "SELECT * FROM " + mTV + " WHERE " + mTPK + " IN (SELECT "
                + mTPK + " FROM " + mT + " WHERE " + onFK + " IS NULL) ORDER BY " + oBC + ";";
        res = state.executeQuery(sql);
        return res;
    }
}
