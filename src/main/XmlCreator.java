package main;

import containers.MainFrame;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author jawiedne
 */
public class XmlCreator {

    String xmlDeclaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<!DOCTYPE schema [\n"
            + "\n"
            + "<!ELEMENT schema (main_table, description_table+)>\n"
            + "<!ELEMENT main_table (main_table_id, view)>\n"
            + "<!ELEMENT description_table (description_table_id, relation_to_main_table, view, display_name, work_mode)>\n"
            + "<!ELEMENT view (#PCDATA)>\n"
            + "<!ELEMENT main_table_id (#PCDATA)>\n"
            + "<!ELEMENT description_table_id (#PCDATA)>\n"
            + "<!ELEMENT relation_to_main_table (one_to_n|m_to_n)>\n"
            + "<!ELEMENT one_to_n (main_table_fk)>\n"
            + "<!ELEMENT m_to_n (m_to_n_table, extra_description_column*)>\n"
            + "<!ELEMENT main_table_fk (#PCDATA)>\n"
            + "<!ELEMENT m_to_n_table EMPTY>\n"
            + "<!ELEMENT display_name (#PCDATA)>\n"
            + "<!ELEMENT work_mode EMPTY>\n"
            + "<!ELEMENT extra_description_column EMPTY>\n"
            + "\n"
            + "<!ATTLIST schema name CDATA #REQUIRED>\n"
            + "<!ATTLIST main_table name CDATA #REQUIRED>\n"
            + "<!ATTLIST description_table name CDATA #REQUIRED>\n"
            + "<!ATTLIST m_to_n_table name CDATA #REQUIRED>\n"
            + "<!ATTLIST work_mode type (search|search_and_edit) #REQUIRED>\n"
            + "<!ATTLIST extra_description_column name CDATA #REQUIRED>\n"
            + "]>";
    File file;
    FileWriter writer;
    MainFrame parent;

    /**
     * Create a new XmlCreator.
     * 
     * @param parent 
     */
    public XmlCreator(MainFrame parent) {

        this.parent = parent;
    }

    /**
     * Save a XML-file with the dtd already in it.
     * 
     * @throws IOException 
     */
    public void save() throws IOException {

        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("xml", "xml"));
        int showSaveDialog = fc.showSaveDialog(parent);
        if (showSaveDialog == JFileChooser.APPROVE_OPTION) {
            
            file = fc.getSelectedFile();
            writer = new FileWriter(file);
            writer.write(xmlDeclaration);
            writer.flush();
            writer.close();
        }
    }
}
