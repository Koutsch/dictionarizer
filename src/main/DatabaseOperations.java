/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import beans.Table;
import components.ColumnPanel;
import components.WorkingPanel;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * All the methods which WorkingPanel and DictionaryPanel and/or other
 * components use commonly.
 *
 * @author jawiedne
 */
public class DatabaseOperations {

    /**
     * Create a new DatabaseOperations instance.
     */
    public DatabaseOperations() {
    }

    /**
     * Transforms JTable rows into an Object[][].
     *
     * @param rIs the indexes of the rows to be transformed
     * @param cIs the indexes of the columns to take the values from
     * @param rT the JTable to take values from
     * @return an Object[][] representing the table row data
     */
    private static Object[][] getRowsFromTable(int[] rIs, int[] cIs, JTable rT) {

        Object[][] tableRows = new Object[rIs.length][];
        for (int i = 0; i < rIs.length; i++) {

            Object[] primaryKeyValues = new Object[cIs.length];
            for (int j = 0; j < cIs.length; j++) {

                primaryKeyValues[j] = rT.getValueAt(rIs[i], cIs[j]);
            }
            tableRows[i] = primaryKeyValues;
        }
        return tableRows;
    }

    /**
     * Inserts a single record (row) into the database. The data in the
     * columnSearchFields are inserted into the respective columns. If the user
     * doesn't fill out a text field than a null value in the respective cell is
     * created.
     *
     * @param cps the ColumnPanels carrying the values to insert
     * @param dc the DatabaseConnector connected with the database
     * @param t the name of the database table
     * @param dtm the table model to display the newly inserted at
     * @param aIC the name of the auto increment column (if available)
     * @return either a String with the auto generated key or an error message.
     *
     */
    public static String insert(ColumnPanel[] cps, DatabaseConnector dc, String t,
            DefaultTableModel dtm, String aIC) {

        try {
            java.util.HashMap<String, String> columnsToInsert = new java.util.HashMap<>();
            for (ColumnPanel cp : cps) {
                if (!cp.getText().isEmpty()) {
                    columnsToInsert.put(cp.getColumnName(), cp.getText());
                }
            }

            if (!columnsToInsert.isEmpty()) {
                int autoKey = dc.insert(t, columnsToInsert);
                if (autoKey != -1 && aIC != null) {
                    ResultSet newRow = dc.search(t, aIC, autoKey, null);
                    Object[] rowArr = new Object[cps.length];
                    if (newRow.next()) {
                        for (int i = 0; i < cps.length; i++) {
                            rowArr[i] = newRow.getObject(i + 1);
                        }
                        dtm.addRow(rowArr);
                        return "Row inserted! Returned key: " + autoKey;
                    }
                    return "Row not found! Please check!";
                }
                return "Row inserted! - No generated key received, search to display result!";
            }
            return "No columns to insert!";

        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "Basic elements not loaded! Check XML file!\n"
                    + ex, "Error", JOptionPane.ERROR_MESSAGE);
            return "Error!";

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
            return "An error occurred while adding entry!";
        }
    }

    /**
     * Performs an update statement. Those columnTextFields which are not empty
     * will be recognized and the updates will performed at all resultTable rows
     * selected. The change is also made visible in the result table.
     *
     * @param rT the JTable representing the database table to update
     * @param t the name of the database table
     * @param dc the DatabaseConnector connected with the database
     * @param pkIs the indexes of the primary key tables
     * @param cps the ColumnPanels carrying the values to update
     *
     * @return a String stating whether update was successful or error occurred.
     */
    public static String update(JTable rT, String t, DatabaseConnector dc, int[] pkIs, ColumnPanel[] cps) {

        try {

            if (rT.getSelectedRows().length != 0) {

                int[] selectedRows = rT.getSelectedRows();
                Object[][] primaryKeyRowValues = getRowsFromTable(selectedRows, pkIs, rT);

                java.util.HashMap<String, Object> columnsToUpdate = new java.util.HashMap<>();
                for (int i = 0; i < cps.length; i++) {
                    if (!cps[i].getText().isEmpty()) {
                        columnsToUpdate.put(cps[i].getColumnName(), cps[i].getText());
                        for (int selectedRow : selectedRows) {

                            rT.setValueAt(cps[i].getText(), selectedRow, i);
                        }
                    }
                }

                String[] primaryKeyColumnNames = new String[pkIs.length];
                for (int i = 0; i < primaryKeyColumnNames.length; i++) {

                    primaryKeyColumnNames[i] = cps[pkIs[i]].getColumnName();
                }

                dc.update(t, primaryKeyRowValues, primaryKeyColumnNames, columnsToUpdate);
                return "Update successful!";

            } else {

                String message = "Select at least one row!";
                JOptionPane.showMessageDialog(null, message);
                return message;
            }
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, ex);
            return ex.getSQLState();
        }
    }

    /**
     * Adds a single value to a specified column of a table.
     *
     * @param rT the JTable the values come from
     * @param t the name of the database table
     * @param dc the DatabaseConnector connected with the database
     * @param tbl the Table object holding the column information
     * @param v the value to be put. If null, the respective row cell is set
     * null in the database
     * @param cTU the column the value to be updated in
     * @throws SQLException
     */
    public static void update(JTable rT, String t, DatabaseConnector dc,
            Table tbl, Object v, String cTU) throws SQLException {

        if (rT.getSelectedRows().length != 0) {

            int[] primaryKeyIndices = tbl.getPrimaryKeyIndices();
            Object[][] primaryKeyRowValues = getRowsFromTable(rT.getSelectedRows(), primaryKeyIndices, rT);

            String[] primaryKeyColumnNames = new String[primaryKeyIndices.length];
            for (int i = 0; i < primaryKeyColumnNames.length; i++) {

                primaryKeyColumnNames[i] = tbl.getColumns()[primaryKeyIndices[i]].getName();
            }

            if (v != null) {

                java.util.HashMap<String, Object> columnsToUpdate = new java.util.HashMap<>();
                columnsToUpdate.put(cTU, v);
                dc.update(t, primaryKeyRowValues, primaryKeyColumnNames, columnsToUpdate);
            } else {

                dc.setCellToNull(t, primaryKeyRowValues, primaryKeyColumnNames, cTU);
            }

        } else {

            JOptionPane.showMessageDialog(null, "Select at least one row!");
        }
    }

    /**
     * Creates entries in a m:n-table.
     *
     * @param wpM the WorkingPanel holding the m-values
     * @param wpN the WorkingPanel holding the n-value
     * @param rCM the name of the referenced m-column
     * @param rCN the name of the referenced n-column
     * @param dc the DatabaseConnector connected with the database
     * @param mTnT the name of the m:n-table
     * @param cM the m-table referencing column
     * @param cN the n-table referencing column
     * @return a String holding all values.
     *
     * @throws SQLException
     */
    public static String connectNtoMRows(WorkingPanel wpM, WorkingPanel wpN, Object rCM,
            Object rCN, DatabaseConnector dc, String mTnT, Object cM,
            Object cN) throws SQLException {

        Object[] valuesM = wpM.getCellsFromSelected((String) rCM);
        Object valueN = wpN.getCellFromSelected((String) rCN);

        if (valuesM != null && valueN != null) {
            dc.insertNtoMConnections(mTnT, (String) cM, (String) cN, valuesM, valueN);
            StringBuilder sb = new StringBuilder();
            for (Object valueM : valuesM) {

                sb.append(valueM).append(" ");
            }
            sb.append("> ").append(valueN);
            return sb.toString();

        } else {

            JOptionPane.showMessageDialog(null, "No selection made!");
            return "";
        }
    }

    /**
     * Deletes the selected rows. If no rows are selected this method prompts
     * the user to select at least one row. After pressing the 'delete' button a
     * "Are you sure?"-option pane occurs.
     *
     * @param rT the JTable representing the database to delete from
     * @param t the name of the database table to delete from
     * @param dc the DatabaseConnector connected with the database
     * @param tbl the Table object holding the column information
     * @param m the DefaultTableModel of the result table where the deleted
     * entries are removed from
     *
     * @return a String stating whether deletion was successful, canceled or an
     * error occurred.
     */
    public static String delete(JTable rT, String t, DatabaseConnector dc,
            Table tbl, DefaultTableModel m) {

        try {
            if (rT.getSelectedRows().length != 0) {

                int[] primaryKeyIndices = tbl.getPrimaryKeyIndices();
                int option = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this?\nIt cannot be undone!",
                        "Deleting entries...", JOptionPane.YES_NO_OPTION);

                if (option == JOptionPane.YES_OPTION) {

                    int[] rowIndexes = rT.getSelectedRows();
                    Object[][] deleteRows = getRowsFromTable(rowIndexes, primaryKeyIndices, rT);

                    String[] primaryKeyColumnNames = new String[primaryKeyIndices.length];
                    for (int i = 0; i < primaryKeyColumnNames.length; i++) {

                        primaryKeyColumnNames[i] = tbl.getColumns()[primaryKeyIndices[i]].getName();
                    }

                    dc.delete(t, deleteRows, primaryKeyColumnNames);
                    for (int i = rowIndexes.length; i > 0; i--) {

                        m.removeRow(rowIndexes[i - 1]);
                    }
                    return "Successfully deleted!";
                }

                return "Deletion cancelled by user.";
            } else {

                String message = "Select at least one row!";
                JOptionPane.showMessageDialog(null, message);
                return message;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            return ex.getSQLState();
        }
    }

    /**
     * Gets a cell from a row with a certain row index.
     *
     * @param columnName the name of the column
     * @param rT the result table
     * @param m the table model
     * @return an Object representing the table row data
     */
    public static Object getCellFromSelected(String columnName, JTable rT, DefaultTableModel m) {

        if (rT.getSelectedRowCount() != 0) {

            int rowIndex = rT.getSelectedRow();

            int columnIndex = m.findColumn(columnName);
            Object cell = rT.getValueAt(rowIndex, columnIndex);
            return cell;
        } else {

            return null;
        }
    }
}
