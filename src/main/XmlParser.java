package main;

import beans.DescriptionTables;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jawiedne
 */
public class XmlParser {

    javax.xml.parsers.DocumentBuilderFactory docBuilderFactory;
    javax.xml.parsers.DocumentBuilder docBuilder;
    org.w3c.dom.Document doc;
    
    public static final String searchMode = "search";
    public static final String searchEditMode = "search_and_edit";

    public XmlParser(String fileName) throws ParserConfigurationException, SAXException, IOException {

        docBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
        docBuilder = docBuilderFactory.newDocumentBuilder();
        doc = docBuilder.parse(new java.io.File(fileName));
        doc.getDocumentElement().normalize();
    }

    private String getChildElementValue(Element parent, String tagName) {

        NodeList childList = parent.getElementsByTagName(tagName);
        Element childElement = (Element) childList.item(0);
        NodeList grandchildList = childElement.getChildNodes();
        return grandchildList.item(0).getNodeValue();
    }

    private String getChildElementAttribute(Element parent, String tagName, String attName) {

        NodeList childList = parent.getElementsByTagName(tagName);
        Element childElement = (Element) childList.item(0);
        return childElement.getAttribute(attName);
    }
    
    private String[] getChildElementsAttribute(Element parent, String tagName, String attName) {
        
        NodeList childList = parent.getElementsByTagName(tagName);
        String[] values = new String[childList.getLength()];
        for (int i = 0; i < values.length; i++) {
            
            Element childElement = (Element) childList.item(i);
            values[i] = childElement.getAttribute(attName);
        }
        return values;
    }

    /**
     * Gets the necessary specifications from an XML file for setting up the
     * main table in the dictionary mode.
     *
     * @return a String[] with length 3, holding at [0] the name of the main
     * table, at[1] the name name of the primary key id column and at [2] the
     * name of the table view to be displayed in the main table resultTable
     */
    public String[] getMainTableSpecifications() {

        String[] mts = new String[3];

        org.w3c.dom.NodeList mainTableList = doc.getElementsByTagName("main_table");

        org.w3c.dom.Node mainTableNode = mainTableList.item(0);
        org.w3c.dom.Element mainTable = (org.w3c.dom.Element) mainTableNode;
        mts[0] = mainTable.getAttribute("name");

        mts[1] = getChildElementValue(mainTable, "main_table_id");
        mts[2] = getChildElementValue(mainTable, "view");
        return mts;
    }

    /**
     * Gets the necessary specifications from an XML file for setting up the
     * description tables in the dictionary mode.
     *
     * @return a DescriptionTables object holding: 
     * String [][] holding all description tables, the respective
     * String[]s holding: [0] table name, [1] table primary key id column, [2]
     * id column used for searches and connections between main and description
     * tables (either main table foreign key or description table primary key id
     * column), [3] the m:n table connecting main and description table, null if
     * it's a 1:n relation, [4] the view for the description table used for the 
     * depending results table, [5] the String that should be displayed at the
     * tabbed pane, [6] "search" if the description tables should read only,
     * "search_and_edit" if they also should be addable and updateable; and:
     * a String[][] holding all extra description columns related to the description 
     * tables; a null value means that there is no extra description column 
     */
    public DescriptionTables getDescriptionTableSpecifications() {

        int dtCnt = doc.getElementsByTagName("description_table").getLength();
        String[][] dts = new String[dtCnt][];
        String[][] enmd = new String[dtCnt][];
        org.w3c.dom.NodeList descriptionTableList = doc.getElementsByTagName("description_table");
        for (int i = 0; i < dts.length; i++) {

            String[] dtsElement = new String[7];

            org.w3c.dom.Node descriptionTableNode = descriptionTableList.item(i);
            org.w3c.dom.Element descriptionTable = (org.w3c.dom.Element) descriptionTableNode;
            dtsElement[0] = descriptionTable.getAttribute("name");

            dtsElement[1] = getChildElementValue(descriptionTable, "description_table_id");

            NodeList relationToMainTableList = descriptionTable.getElementsByTagName("relation_to_main_table");
            Node relationToMainTableNode = relationToMainTableList.item(0);
            Element relationToMainTable = (Element) relationToMainTableNode;
            NodeList toNList = relationToMainTable.getElementsByTagName("*");
            Node toNNode = toNList.item(0);
            String toNName = toNNode.getNodeName();
            Element toN = (Element) toNNode;
            if (toNName.equals("one_to_n")) {

                dtsElement[2] = getChildElementValue(toN, "main_table_fk");
                dtsElement[3] = null;
                enmd[i] = null;
            }
            if (toNName.equals("m_to_n")) {

                dtsElement[2] = dtsElement[1];
                dtsElement[3] = getChildElementAttribute(toN, "m_to_n_table", "name");
                
                String[] extraValues = getChildElementsAttribute(toN, "extra_description_column", "name");
                if (extraValues.length > 0) {
                    
                    enmd[i] = getChildElementsAttribute(toN, "extra_description_column", "name");
                } else {
                    
                    enmd[i] = null;
                }               
            }

            dtsElement[4] = getChildElementValue(descriptionTable, "view");
            dtsElement[5] = getChildElementValue(descriptionTable, "display_name");
            dtsElement[6] = getChildElementAttribute(descriptionTable, "work_mode", "type");
            
            dts[i] = dtsElement;
        }        
        return new DescriptionTables(dts, enmd);
    }
}
